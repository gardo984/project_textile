#!/bin/bash
# *-* encoding : UTF-8 *-*

DOCKERFILE="$(pwd)/docker/Dockerfile.debug"
IMG_NAME='malazo/project_textile_app:latest'
STATIC_ROOT="$(pwd)/static/"
[ $(docker image ls -q ${IMG_NAME} |wc -l) -gt 0 ] && docker image rm -f ${IMG_NAME}
docker image build -t ${IMG_NAME} -f ${DOCKERFILE} \
	--build-arg DEBUG \
	--build-arg MYSQL_USER \
	--build-arg MYSQL_PASSWORD \
	--build-arg MYSQL_HOST \
	--build-arg MYSQL_DATABASE \
	--build-arg MYSQL_PORT \
	--build-arg STATIC_ROOT=${STATIC_ROOT} .
