#!/bin/bash
# *-* encoding : UTF-8 *-*

# the following variables will be available in building process:
# PROJECT_NAME : project name
# PROJECT_DIR : application directory
# JENKINS_DIR : jenkins project application directory

MEDIA_DIR="${PROJECT_DIR}/public/media"

if [ ! -d "${PROJECT_DIR}" ]; then
	sudo mkdir -p ${PROJECT_DIR} && \
	echo "Directory was created ${PROJECT_DIR}"
else
	sudo rm -rf ${PROJECT_DIR} && sudo mkdir -p ${PROJECT_DIR}
	echo "Directory was cleaned ${PROJECT_DIR}"
fi

sudo cp -avR ${WORKSPACE}/* ${PROJECT_DIR}/

if [ -d "${PROJECT_DIR}" ]; then
	# assigning full access to jenkins and apache accounts
	sudo chown jenkins:apache ${PROJECT_DIR} && sudo chown jenkins:apache -R ${PROJECT_DIR}
	sudo chmod 0774 -R ${PROJECT_DIR} && echo "Performing grants on ${PROJECT_DIR}"
	sudo chmod +2774 ${PROJECT_DIR} && \
	sudo chmod +2774 -R ${PROJECT_DIR} && echo "Enabling set group ID on ${PROJECT_DIR}"
fi

if [ -d "${MEDIA_DIR}" ]; then
	# set acl permissions for file / dirs generation
	sudo setfacl -m u:jenkins:rwx ${MEDIA_DIR}
	sudo setfacl -m g:apache:rwx ${MEDIA_DIR}
	sudo setfacl -m u:jenkins:rwx -R ${MEDIA_DIR}
	sudo setfacl -m g:apache:rwx -R ${MEDIA_DIR}
	sudo setfacl -d -m u:jenkins:rwx ${MEDIA_DIR}
	sudo setfacl -d -m g:apache:rwx ${MEDIA_DIR}
	sudo setfacl -d -m u:jenkins:rwx -R ${MEDIA_DIR}
	sudo setfacl -d -m g:apache:rwx -R ${MEDIA_DIR}

	echo "ACL permissions have been given ${MEDIA_DIR}"
fi
