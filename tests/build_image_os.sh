#!/bin/bash
# *-* encoding : UTF-8 *-*

DOCKERFILE="$(pwd)/docker/Dockerfile_os"
IMG_NAME='malazo/project_textile_ubuntu:latest'
[ $(docker image ls -q ${IMG_NAME} |wc -l) -gt 0 ] && docker image rm -f ${IMG_NAME}
docker image build -t ${IMG_NAME} -f ${DOCKERFILE} .
