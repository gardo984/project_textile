#!/bin/bash
# *-* encoding : UTF-8 *-* 

CONT_NAME="cont_app_server"
IMG_NAME="malazo/project_textile_app"
[ $(docker ps -a |grep -i ${CONT_NAME} |wc -l) -gt 0 ] && docker rm -f ${CONT_NAME} 
docker run --name ${CONT_NAME} -p 8000:8000 \
	-e DEBUG=${DEBUG} \
	-e MYSQL_USER=${MYSQL_USER} \
	-e MYSQL_PASSWORD=${MYSQL_PASSWORD} \
	-e MYSQL_HOST=${MYSQL_HOST} \
	-e MYSQL_DATABASE=${MYSQL_DATABASE} \
	-e MYSQL_PORT=3306  -d ${IMG_NAME}
