#!/bin/bash
# *-* encoding : UTF-8 *-* 

APP_SETTING="settings.develop"
cd ${PROJECT_DIR}
virtualenv env -p $(which python3.6)
source env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
python public/manage.py makemigrations --merge --noinput --settings=${APP_SETTING}
python public/manage.py migrate --settings=${APP_SETTING}
python public/manage.py collectstatic -c --noinput --settings=${APP_SETTING}
# touch public/settings/wsgi-develop.py
