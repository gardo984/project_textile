from django import forms
import json


class MFDefault(forms.Form):

    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'

    def __init__(self, *args, **kwargs):
        super(MFDefault, self).__init__(*args, **kwargs)

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            if self.fields[wkey].label != None:
                wresult["field"] = self.fields[wkey].label
            else:
                wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field"), wresult.get("message"),
            )
        return wmessage
