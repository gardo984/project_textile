#! /usr/bin/env python3
# -*- encoding:utf8 -*-

from django import forms
from django.contrib.auth import (
    models as mdlauth,
    authenticate,
)
from django.db.models import Q, F
from django.utils.translation import gettext_lazy as _
from apps.catalogos import (
    models as mdlcat,
)
import json

# Model Forms

# Forms

DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'


class MFUser(forms.ModelForm):
    NO_REQUIRED = []

    usuario = forms.CharField(min_length=4, max_length=50, required=True,
                              error_messages={
                                  'no_empty': DEFAULT_MSG_REQUIRED, },
                              )
    clave = forms.CharField(min_length=6, max_length=50, required=True,
                            error_messages={
                                'no_empty': DEFAULT_MSG_REQUIRED, },
                            )

    class Meta:
        model = mdlauth.User
        fields = [
            'first_name', 'last_name', 'email',
            'usuario', 'clave',
        ]
        exclude = []

    def __init__(self, *args, **kwargs):
        super(MFUser, self).__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        wstatus = authenticate(
            username=cleaned_data.get("usuario"),
            password=cleaned_data.get("clave")
        )
        if wstatus is None:
            wmessage = {
                "clave": "no paso validacion",
            }
            raise forms.ValidationError(wmessage)
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                if item in ["email", ]:
                    cleaned_data[item] = cleaned_data.get(item).lower()
                    continue
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wmessage = ''
        if self.errors:
            wresult = {}
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
            wmessage = 'Error, {} : {}.'.format(
                wresult.get("field"), wresult.get("message"),
            )
        return wmessage
