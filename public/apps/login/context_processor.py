#! /usr/bin/env python
# -*- coding:utf-8 -*-

from django.conf import settings as cnf
from django.contrib.sessions import models as mdl


def get_access_menu(request):
    wmenu = []
    if request.session.get("menu"):
        wmenu = request.session.get("menu")

    wexpiration = None
    if request.session.exists(request.session._get_session_key()):
        obj = mdl.Session.objects.filter(
            session_key=request.session._get_session_key()
        )
        if obj.exists():
            wexpiration = {
                "date": obj.first().expire_date,
            }

    return {
        "menu": wmenu,
        "year": cnf.PROJECT_YEAR,
        "env": cnf.DEBUG,
        "company": cnf.PROJECT_COMPANY,
        "expiration": wexpiration,
        "minimun": cnf.PROJECT_SESSION_DEADLINE,
    }
