
from django.db.models import (
    Value, Q, F, Prefetch,
)
from apps.catalogos import models as mdlcat


def get_not_active_instance():
    """ get the not active status instance """
    return (
        mdlcat.Estados.objects.get(
            id__iexact=mdlcat.Estados.Flags.NOT_ACTIVE,
        )
    )


def qs_colors():
    """ get the general queryset of colors """
    return (
        mdlcat.Color.objects.all()
        .select_related("estado")
        .select_related("username")
        .select_related("username_modif")
        .order_by("id")
    )


def qs_sizes():
    """ get the general queryset of sizes """
    return (
        mdlcat.Sizes.objects.all()
        .select_related("estado")
        .select_related("username")
        .select_related("username_modif")
        .order_by("id")
    )


def get_list_sizes(**kwargs):
    """ get the list of sizes """
    outcome, error = None, None
    try:
        data = kwargs.get("data")
        qfilter = Q(id__isnull=False)
        if data.get("descripcion"):
            description = data.get("descripcion") or ''
            qfilter.add(
                Q(descripcion__icontains=description),
                qfilter.connector
            )
        if data.get("estado"):
            status = (
                data.get("estado") or mdlcat.Estados.Flags.NOT_ACTIVE
            )
            qfilter.add(
                Q(estado__id__iexact=status), qfilter.connector
            )
        rows = qs_sizes().filter(qfilter)
        outcome = rows
    except Exception as ex:
        error = str(ex)
    return [outcome, error]


def get_list_colors(**kwargs):
    """ get the list of colors """
    outcome, error = None, None
    try:
        data = kwargs.get("data")
        qfilter = Q(id__isnull=False)
        if data.get("descripcion"):
            description = data.get("descripcion") or ''
            qfilter.add(
                Q(descripcion__icontains=description),
                qfilter.connector
            )
        if data.get("estado"):
            status = (
                data.get("estado") or mdlcat.Estados.Flags.NOT_ACTIVE
            )
            qfilter.add(
                Q(estado__id__iexact=status), qfilter.connector
            )
        rows = qs_colors().filter(qfilter)
        outcome = rows
    except Exception as ex:
        error = str(ex)
    return [outcome, error]
