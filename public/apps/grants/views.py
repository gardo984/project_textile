
import os
import json
from django.contrib.auth import models as mdlauth
from django.shortcuts import render, redirect
from django.conf import settings
from django.views import View
from django.http import (
    JsonResponse, Http404,
)
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from django.db.models import (
    Value, Q, F,
)
from apps.catalogos import (
    models as mdlcat,
    serializers as srlzcat,
)
from apps.grants import (
    forms as frm, utils as utlgr,
)
from apps.common import utils as utl
from apps.login import serializers as srlzlog

# Create your views here.

DEFAULT_MAIN_PAGE = '/main/'
if settings.DEBUG is False:
    DEFAULT_MAIN_PAGE = os.path.join(settings.PREFIX_URL, 'main')

DEFAULT_RESPONSE = {
    "ok": 1,
    "status": 200,
    "result": "Request has been processed successfuly.",
}

BRAND = settings.PROJECT_COMPANY

# Color process


@method_decorator(login_required, name='dispatch')
class ColorView(View):
    """ class based view for Color module main interface """
    template_name = 'grants/grants_main.html'
    template_title = 'de Color'
    template_header = '{} | Mantenimiento {}'.format(
        BRAND, template_title
    )

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.display_color', raise_exception=True),)
    def get(self, request):
        """ handle get method requests """
        header, title = self.template_header, self.template_title
        params = {"header": header, "title": title}
        return render(request, self.template_name, params)

    @method_decorator(permission_required(
        'catalogos.display_color', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            fields = [
                'id', 'descripcion',
                'fhregistro', 'fhmodificacion', 'estado',
                'username', 'username_modif',
            ]
            params = {
                "data": wdata, "user": request.user,
            }
            rows, error = utlgr.get_list_colors(**params)
            if error:
                raise ValueError(str(error))
            page["total"] = rows.count()
            wserial = srlzcat.ColorSerializer(
                rows[wfrom:wto], fields=fields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ColorUpdateView(View):
    """ class based view for sizes module update functionality """

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.change_color', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFColor(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            objitem = mdlcat.Color.objects.filter(
                id=wdata.get("id")
            )
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, Color especificado no existe.'))

            # objitem = objitem.first()
            wparameters.update({
                "fhmodificacion": timezone.now(),
                "username_modif": request.user,
            })
            objitem.update(**wparameters)

            wresponse["result"] = {
                'id': objitem.first().pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ColorRemoveView(View):
    """ class based view for sizes module removal functionality """

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.delete_color', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilter = Q(id__isnull=False)
            if wdata.get("id"):
                qfilter.add(Q(id=wdata.get("id")), qfilter.connector)
            objitem = mdlcat.Color.objects.filter(qfilter)
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, talla especificado no existe.'))

            objitem = objitem.first()
            objitem.estado = utlgr.get_not_active_instance()
            objitem.save()
            wresponse["result"] = {
                'message': 'Color fue desactivado satisfactoriamente.',
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ColorAppendView(View):
    """ class based view for sizes module registration functionality """

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.add_color', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFColor(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            wparameters["username"] = request.user
            objitem = mdlcat.Color(**wparameters)
            objitem.save()

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

# Sizes process


@method_decorator(login_required, name='dispatch')
class SizesView(View):
    """ class based view for Sizes module main interface """
    template_name = 'grants/grants_main.html'
    template_title = 'de Tallas'
    template_header = '{} | Mantenimiento {}'.format(
        BRAND, template_title
    )

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.display_sizes', raise_exception=True),)
    def get(self, request):
        """ handle get method requests """
        header, title = self.template_header, self.template_title
        params = {"header": header, "title": title}
        return render(request, self.template_name, params)

    @method_decorator(permission_required(
        'catalogos.display_sizes', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            fields = [
                'id', 'descripcion',
                'fhregistro', 'fhmodificacion', 'estado',
                'username', 'username_modif',
            ]
            params = {
                "data": wdata, "user": request.user,
            }
            rows, error = utlgr.get_list_sizes(**params)
            if error:
                raise ValueError(str(error))
            page["total"] = rows.count()
            wserial = srlzcat.SizesSerializer(
                rows[wfrom:wto], fields=fields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SizesUpdateView(View):
    """ class based view for sizes module update functionality """

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.change_sizes', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFSizes(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            objitem = mdlcat.Sizes.objects.filter(
                id=wdata.get("id")
            )
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, Talla especificado no existe.'))

            # objitem = objitem.first()
            wparameters.update({
                "fhmodificacion": timezone.now(),
                "username_modif": request.user,
            })
            objitem.update(**wparameters)

            wresponse["result"] = {
                'id': objitem.first().pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SizesRemoveView(View):
    """ class based view for sizes module removal functionality """

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.delete_sizes', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilter = Q(id__isnull=False)
            if wdata.get("id"):
                qfilter.add(Q(id=wdata.get("id")), qfilter.connector)
            objitem = mdlcat.Sizes.objects.filter(qfilter)
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, talla especificado no existe.'))

            objitem = objitem.first()
            objitem.estado = utlgr.get_not_active_instance()
            objitem.save()
            wresponse["result"] = {
                'message': 'Talla fue desactivado satisfactoriamente.',
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SizesAppendView(View):
    """ class based view for sizes module registration functionality """

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.add_sizes', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFSizes(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            wparameters["username"] = request.user
            objitem = mdlcat.Sizes(**wparameters)
            objitem.save()

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

# Sellers process


@method_decorator(login_required, name='dispatch')
class SellersView(View):
    """ class based view for seller module main interface """
    template_name = 'grants/grants_main.html'
    template_title = 'de Vendedores'
    template_header = 'Invoice | Mantenimiento {}'.format(template_title)

    def dispatch(self, *args, **kwargs):
        return super(SellersView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.display_vendedores', raise_exception=True),)
    def get(self, request):
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'catalogos.display_vendedores', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            qfilter = Q(id__isnull=False)
            if wdata.get("nombres"):
                wdescription = wdata.get("nombres") or ''
                qfilter.add(
                    Q(nombres__icontains=wdescription), qfilter.connector
                )
            if wdata.get("estado"):
                wstatus = wdata.get("estado") or 2
                qfilter.add(
                    Q(estado__id__iexact=wstatus), qfilter.connector
                )

            wfields = [
                'id', 'nombres', 'fhregistro', 'fhmodificacion',
                'estado',
                'username', 'username_modif',
            ]
            wresult = mdlcat.Vendedores.objects.filter(qfilter) \
                .select_related("estado") \
                .select_related("username") \
                .select_related("username_modif") \
                .order_by("id")

            page["total"] = wresult.count()
            wserial = srlzcat.VendedoresSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SellersUpdateView(View):
    """ class based view for seller module update functionality """

    def dispatch(self, *args, **kwargs):
        return super(SellersUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.change_vendedores', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFVendedores(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            objitem = mdlcat.Vendedores.objects.filter(
                id=wdata.get("id")
            )
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, vendedor especificado no existe.'))

            # objitem = objitem.first()
            wparameters.update({
                "fhmodificacion": timezone.now(),
                "username_modif": request.user,
            })
            objitem.update(**wparameters)

            wresponse["result"] = {
                'id': objitem.first().pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SellersRemoveView(View):
    """ class based view for seller module removal functionality """

    def dispatch(self, *args, **kwargs):
        return super(SellersRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.delete_vendedores', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilter = Q(id__isnull=False)
            if wdata.get("id"):
                qfilter.add(Q(id=wdata.get("id")), qfilter.connector)
            objitem = mdlcat.Vendedores.objects.filter(qfilter)
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, vendedor especificado no existe.'))

            objitem = objitem.first()
            objitem.estado = utlgr.get_not_active_instance()
            objitem.save()
            wresponse["result"] = {
                'message': 'Vendedor fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class SellersAppendView(View):
    """ class based view for seller module registration functionality """

    def dispatch(self, *args, **kwargs):
        return super(SellersAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.add_vendedores', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFVendedores(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            wparameters["username"] = request.user
            objitem = mdlcat.Vendedores(**wparameters)
            objitem.save()

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

# Profile process


@method_decorator(login_required, name='dispatch')
class ProfileView(View):
    """ class based view for group module main interface """
    template_name = 'grants/grants_main.html'
    template_title = 'Perfiles de Sistema'
    template_header = 'Invoice | Mantenimiento {}'.format(template_title)

    def dispatch(self, *args, **kwargs):
        return super(ProfileView, self).dispatch(*args, **kwargs)

    def get(self, request):
        """ function to handle get requests """
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            qfilter = Q(id__isnull=False)
            if wdata.get("descripcion"):
                wdescription = wdata.get("descripcion") or ''
                qfilter.add(
                    Q(name__icontains=wdescription) |
                    Q(id__icontains=wdescription),
                    qfilter.connector
                )
            if wdata.get("estado"):
                wstatus = 1 if wdata.get("estado") == 1 else 0
                qfilter.add(
                    Q(groupextension__is_active=wstatus),
                    qfilter.connector
                )

            wfields = ['id', 'name', 'groupextension', ]
            wresult = mdlauth.Group.objects.filter(qfilter) \
                .select_related("groupextension") \
                .order_by("id")

            page["total"] = wresult.count()
            wserial = srlzlog.GroupSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileUpdateView(View):
    """ class based view for group module update functionality """

    def dispatch(self, *args, **kwargs):
        return super(ProfileUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.change_group', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFGroup(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            objitem = mdlauth.Group.objects.filter(id=wdata.get("id"))
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, grupo especificado no existe.'))
            objitem = objitem.first()
            objitem.name = wparameters.get("name")
            if hasattr(objitem, 'groupextension'):
                wstatus = 1 if wparameters.get("estado") == 1 else 0
                objitem.groupextension.is_active = wstatus
            objitem.save()

            if wdata.get("permissions"):
                objitem.permissions.remove(*objitem.permissions.all())
                wperms = mdlauth.Permission.objects.filter(
                    id__in=wdata.get("permissions") or []
                )
                if wperms.count() > 0:
                    objitem.permissions.add(*wperms)

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron procesados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileRemoveView(View):
    """ class based view for group module removal functionality """

    def dispatch(self, *args, **kwargs):
        return super(ProfileRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.delete_user', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilter = Q(id__isnull=False)
            if wdata.get("id"):
                qfilter.add(Q(id=wdata.get("id")), qfilter.connector)
            objitem = mdlauth.Group.objects.filter(qfilter)
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(str('Error, perfil especificado no existe.'))
            objitem = objitem.first()
            if hasattr(objitem, 'groupextension'):
                objitem.groupextension.is_active = 0
            objitem.save()
            wresponse["result"] = {
                'message': 'Concesionario fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ProfileAppendView(View):
    """ class based view for group module registration functionality """

    def dispatch(self, *args, **kwargs):
        return super(ProfileAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.add_group', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            form = frm.MFGroup(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())
            wparameters = form.clean()
            # wparameters["username"] = request.user
            objitem = mdlauth.Group()
            objitem.name = wparameters.get("name")
            objitem.save()
            if hasattr(objitem, 'groupextension'):
                wstatus = 1 if wparameters.get("estado") == 1 else 0
                objitem.groupextension.is_active = wstatus
                objitem.groupextension.save()

            if wdata.get("permissions"):
                wperms = mdlauth.Permission.objects.filter(
                    id__in=wdata.get("permissions") or []
                )
                if wperms.count() > 0:
                    objitem.permissions.add(*wperms)

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


# User process


@method_decorator(login_required, name='dispatch')
class UserView(View):
    """ class based view for user module main interface """
    template_name = 'grants/grants_usuarios.html'
    template_title = 'Usuarios de Sistema'
    template_header = 'Invoice | Mantenimiento {}'.format(template_title)

    def dispatch(self, *args, **kwargs):
        return super(UserView, self).dispatch(*args, **kwargs)

    def get(self, request):
        """ function to handle get requests """
        header, title = self.template_header, self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            qfilter = Q(id__isnull=False)
            if wdata.get("descripcion"):
                wdescription = wdata.get("descripcion") or ''
                qfilter.add(
                    Q(first_name__icontains=wdescription) |
                    Q(last_name__icontains=wdescription) |
                    Q(username__exact=wdescription),
                    qfilter.connector
                )
            if wdata.get("estado"):
                westado = 1 if wdata.get("estado") == 1 else 0
                qfilter.add(
                    Q(is_active=westado), qfilter.connector
                )
            wfields = [
                'id', 'username', 'first_name', 'last_name',
                'email', 'userextension', 'fullname',
                'is_active', 'date_joined',
            ]
            wresult = mdlauth.User.objects.filter(qfilter) \
                .order_by("first_name") \
                .select_related("userextension")

            page["total"] = wresult.count()
            wserial = srlzlog.UserSerializer(
                wresult[wfrom:wto], fields=wfields, many=True,
            ).data
            wresponse["result"] = {
                "rows": wserial,
                "page": page,
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserUpdateView(View):
    """ class based view for user module update functionality """

    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.change_user', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            wdata.get("fields")["create"] = 0
            form = frm.MFUser(wdata.get("fields"))
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())

            wparam = form.clean()
            objitem = mdlauth.User.objects.filter(
                username__iexact=wparam.get("usuario"))
            ufields = [
                "first_name", "last_name", "email",
                "username", "is_active",
            ]
            if objitem.exists():
                objitem.update(**{x: wparam.get(x) for x in ufields})
                objitem = objitem.first()
                # if new password was typed
                if wparam.get("password") != '':
                    objitem.set_password(wparam.get("password"))
                    objitem.save(update_fields=["password", ])
                # update the extended table of user
                if hasattr(objitem, 'userextension'):
                    objitem.userextension.dni = wparam.get("dni")
                    objitem.userextension.all_customers = wparam.get(
                        "all_customers")
                    objitem.userextension.all_orders = wparam.get(
                        "all_orders")
                    objitem.userextension.fhmodificacion = timezone.now()
                    objitem.userextension.save(
                        update_fields=[
                            "dni", "fhmodificacion", "all_customers",
                            "all_orders",
                        ])

            # update grants
            status, error = update_grants(objitem, request)
            if error:
                wresponse['status'] = 400
                raise ValueError(str(error))

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserRemoveView(View):
    """ class based view for user module removal functionality """

    def dispatch(self, *args, **kwargs):
        return super(UserRemoveView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.delete_user', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilter = Q(id__isnull=False)
            if wdata.get("id"):
                qfilter.add(Q(id=wdata.get("id")), qfilter.connector)
            objitem = mdlauth.User.objects.filter(qfilter)
            if not objitem.exists():
                wresponse['status'] = 422
                raise ValueError(
                    str('Error, usuario especificado no existe.'))
            objitem = objitem.first()
            objitem.is_active = 0
            objitem.save()

            wresponse["result"] = {
                'message': 'Concesionario fue desactivado satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class UserAppendView(View):
    """ class based view for user module registration functionality """

    def dispatch(self, *args, **kwargs):
        return super(UserAppendView, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'auth.add_user', raise_exception=True),)
    def post(self, request):
        """ function to handle post requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            wdata.get("fields")["create"] = 1
            form = frm.MFUser(wdata.get("fields"))
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            if not form.is_valid():
                wresponse['status'] = 400
                raise ValueError(form.get_error_first())

            wparam = form.clean()
            ufields = [
                "first_name", "last_name", "email",
                "username", "is_active",
            ]
            objitem = mdlauth.User(**{x: wparam.get(x) for x in ufields})
            objitem.set_password(wparam.get("clave"))
            objitem.save()
            if hasattr(objitem, 'userextension'):
                objitem.userextension.dni = wparam.get("dni")
                objitem.userextension.all_customers = wparam.get(
                    "all_customers")
                objitem.userextension.all_orders = wparam.get(
                    "all_orders")
                objitem.userextension.fhmodificacion = timezone.now()
                objitem.userextension.save(
                    update_fields=["dni", "fhmodificacion",
                                   "all_customers", "all_orders", ]
                )
            # update grants
            status, error = update_grants(objitem, request)
            if error:
                wresponse['status'] = 400
                raise ValueError(str(error))

            wresponse["result"] = {
                'id': objitem.pk,
                'message': 'Datos fueron registrados satisfactoriamente.',
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'], wresponse['result'] = 0, str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


def update_grants(item, request):
    """ function to update grants per user """
    status, error = True, None
    try:
        wdata = json.loads(request.body.decode("utf8"))
        # user permissions
        wperms = mdlauth.Permission.objects.filter(
            id__in=wdata.get("permissions") or []
        )
        item.user_permissions.remove(
            *item.user_permissions.all())
        if wperms.count() > 0:
            item.user_permissions.add(*wperms)

        # group permissions member
        wgroups = mdlauth.Group.objects.filter(
            id__in=wdata.get("groups") or []
        )
        item.groups.remove(*item.groups.all())
        if wgroups.count() > 0:
            item.groups.add(*wgroups)

        # # clientes permissions
        # wclientes = mdlcat.Clientes.objects.filter(
        #     id__in=wdata.get("clientes") or []
        # )
        # item.catalogos_usercliente_user_set.all().delete()
        # if wclientes.count() > 0:
        #     objclientes = [mdlcat.UserCliente(
        #         cliente=x, user=item,
        #         fhregistro=timezone.now(),
        #         username=request.user,
        #     ) for x in wclientes]
        #     item.catalogos_usercliente_user_set.bulk_create(
        #         objclientes, wclientes.count()
        #     )

        status = True
    except Exception as e:
        status, error = False, str(e)
    return [status, error]
