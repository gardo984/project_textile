from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.grants import views as v

app_name = 'module_grants'

urlpatterns = [
    url(r'^sellers/',
        include([
            url('^$', v.SellersView.as_view(), name="sellers_main"),
            url('^update/?$',
                v.SellersUpdateView.as_view(), name="sellers_update"),
            url('^remove/?$',
                v.SellersRemoveView.as_view(), name="sellers_remove"),
            url('^append/?$',
                v.SellersAppendView.as_view(), name="sellers_append"),
        ])),
    url(r'^profile/',
        include([
            url('^$', v.ProfileView.as_view(), name="profile_main"),
            url('^update/?$',
                v.ProfileUpdateView.as_view(), name="profile_update"),
            url('^remove/?$',
                v.ProfileRemoveView.as_view(), name="profile_remove"),
            url('^append/?$',
                v.ProfileAppendView.as_view(), name="profile_append"),
        ])),
    url(r'^users/',
        include([
            url('^$', v.UserView.as_view(), name="users_main"),
            url('^update/?$',
                v.UserUpdateView.as_view(), name="users_update"),
            url('^remove/?$',
                v.UserRemoveView.as_view(), name="users_remove"),
            url('^append/?$',
                v.UserAppendView.as_view(), name="users_append"),
        ])),
    url(r'^sizes/',
        include([
            url('^$', v.SizesView.as_view(), name="sizes_main"),
            url('^update/?$',
                v.SizesUpdateView.as_view(), name="sizes_update"),
            url('^remove/?$',
                v.SizesRemoveView.as_view(), name="sizes_remove"),
            url('^append/?$',
                v.SizesAppendView.as_view(), name="sizes_append"),
        ])),
    url(r'^color/',
        include([
            url('^$', v.ColorView.as_view(), name="color_main"),
            url('^update/?$',
                v.ColorUpdateView.as_view(), name="color_update"),
            url('^remove/?$',
                v.ColorRemoveView.as_view(), name="color_remove"),
            url('^append/?$',
                v.ColorAppendView.as_view(), name="color_append"),
        ])),
]
