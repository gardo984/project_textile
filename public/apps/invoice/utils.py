
import os
from datetime import datetime
# To write and create xls files from zero
from shutil import copyfile
import xlsxwriter as xls
# django settings info
from django.conf import settings
from django.db.models import (
    Q, F, IntegerField, CharField, Count,
    IntegerField, Prefetch, Case, When,
    Subquery, ExpressionWrapper, OuterRef,
)
from django.db.models.functions import (
    Cast,
)
# To generate and modify a new xls file
from openpyxl import load_workbook
from openpyxl.styles import (
    PatternFill, Border, Side,
    Alignment, Protection, Font,
    NamedStyle, Color, Fill,
)
from apps.common.utils import (
    Validators, ExportToExcel,
)
from apps.catalogos import (
    models as mdlcat,
)
from apps.invoice import (
    models as mdls,
)

DATE_FORMAT = "%Y-%m-%d"


def get_backoffice_xls_fields():
    """ get serializer backoffice xls report fields """
    return [
        "id", "order", "fhregistro_format",
        "fhmodificacion_format", "tipo_invoice",
        "cliente", "total", "forma_pago", "fecha_entrega",
        "username", "username_modif",
        "sub_tipo_invoice",
        "tipo_seguimiento",
        "vendedor", "vendedor_nombre",
        "tipo_descrip", "forma_pago_descrip",
        "cliente_phone", "fecha_emision",
        "cliente_nombre", "tipo_seguimiento_descrip",
        "sub_tipo_invoice_descrip",
        "fecha_entrega_reprogramada",
        "pais_descrip",
        "ciudad_descrip",
    ]


def get_backoffice_item_fields():
    """ get serializer backoffice item fields """
    return [
        "id", "cliente", "fhregistro", "fhmodificacion",
        "order", "cliente_phone", "cliente_phone2",
        "cliente_address", "cliente_country", "total",
        "observaciones", "fecha_venta", "fecha_entrega",
        "tipo_invoice", "forma_pago", "estado",
        "nro_tarjeta", "invoicedetails_set",
        "observaciones2", "contesta_celular", "labora",
        "hora_entrega", "delivery_address",
        "sub_tipo_invoice", "tipo_seguimiento",
        "vendedor",
        "fecha_recepcion", "fecha_etd_courier",
        "tarjeta_cvv", "tarjeta_vigencia",
        "fecha_entrega_reprogramada",
        "hora_entrega_reprogramada",
        "hora_entrega_hasta",
        "motivo_reprogramacion",
    ]


def get_backoffice_fields():
    """ get serializer backoffice list fields """
    return [
        "id", "order", "fhregistro_format",
        "fhmodificacion_format", "tipo_invoice",
        "cliente", "total", "forma_pago", "fecha_entrega",
        "username", "username_modif",
        "sub_tipo_invoice",
        "tipo_seguimiento",
        "vendedor", "vendedor_nombre",
        "tipo_descrip", "forma_pago_descrip",
        "cliente_phone", "fecha_emision",
        "cliente_nombre", "tipo_seguimiento_descrip",
        "sub_tipo_invoice_descrip",
        "fecha_entrega_reprogramada",
    ]


def get_orders_fields():
    """ get serializer orders fields """
    return [
        "id", "fhregistro", "fhmodificacion",
        "cliente_ndoc",
        "subtotal", "igv", "total", "cliente",
        "order", "fecha_emision", "fecha_vigencia",
        "username", "estado", "forma_pago",
        "username_modif",
        "ndetails", "tipo_invoice", "sub_tipo_invoice",
    ]


def get_orders_xls_fields():
    """ get serializer orders xls report fields """
    return [
        "id", "order", "fhregistro_format",
        "fecha_emision", "cliente_nombre",
        "cliente_phone", "ndetails",
        "fhmodificacion_format", "tipo_invoice",
        "total", "forma_pago", "fecha_entrega",
        "username", "username_modif",
        "forma_pago_descrip", "tipo_descrip",
        "vendedor_nombre",
        "pais_descrip",
        "ciudad_descrip",
        "fecha_entrega_reprogramada",
    ]


def get_order_item_fields():
    """ get serializer order fields """
    return [
        "id", "cliente", "fhregistro", "fhmodificacion",
        "order", "cliente_phone", "cliente_phone2",
        "cliente_address", "cliente_country", "total",
        "observaciones", "fecha_venta", "fecha_entrega",
        "fecha_emision",
        "tipo_invoice", "forma_pago", "estado",
        "nro_tarjeta", "invoicedetails_set",
        "observaciones2", "contesta_celular", "labora",
        "hora_entrega", "vendedor",
        "fecha_etd_courier", "fecha_recepcion",
        "tarjeta_cvv", "tarjeta_vigencia",
        "hora_entrega_hasta",
        "delivery_address",
    ]


def qs_invoice_details():
    """ get qs order details """
    return (
        mdls.InvoiceDetails.objects.all()
        .select_related("article__categoria__estado")
        .select_related("color__estado")
        .select_related("size__estado")
    )


def qs_invoice_parent():
    """ get qs order parent """
    qscliente = qs_customers()
    qsdetails = qs_invoice_details()
    return (
        mdls.InvoiceParent.objects.all()
        .prefetch_related(
            Prefetch("cliente", queryset=qscliente)
        ).prefetch_related(
            Prefetch("invoicedetails_set", queryset=qsdetails,)
        ).select_related("forma_pago__estado")
        .select_related("tipo_invoice__estado")
        .select_related("sub_tipo_invoice__estado")
        .select_related("tipo_seguimiento__estado")
        .select_related("estado")
        .select_related("username")
        .select_related("username_modif")
        .select_related("vendedor")
    )


def qs_tipo_doc():
    """ get qs document type """
    return (
        mdlcat.TipoDocumento.objects.all()
        .select_related("estado")
    )


def qs_customers():
    """ get qs customers """
    qs_tdoc = qs_tipo_doc()
    return (
        mdlcat.Clientes.objects.all()
        .select_related("pais__estado")
        .select_related("estado")
        .prefetch_related(Prefetch(
            "tdoc", queryset=qs_tdoc,
        ))
    )


def get_invoices_list(**kwargs):
    """ get orders general list from GUI """
    outcome, error = [], None
    try:
        wdata = kwargs

        wcliente = wdata.get('cliente') or ''
        winvoice = wdata.get('invoice') or ''
        wleyenda = wdata.get('sub_tipo_invoice') or ''

        date_from = datetime.strptime(
            wdata.get("fecha_desde"), DATE_FORMAT)
        date_to = datetime.strptime(wdata.get("fecha_hasta"), DATE_FORMAT)
        qfilters = Q(
            id__isnull=False,
            fregistro__range=[date_from, date_to],
        )

        objuser = wdata.get("user")
        if hasattr(objuser, 'userextension'):
            if objuser.userextension.all_orders != 1:
                qfilters.add(Q(username=objuser,), qfilters.connector)
        else:
            qfilters.add(Q(username=objuser,), qfilters.connector)

        if wcliente != '':
            sentence = (
                Q(cliente__razon_social__icontains=wcliente) |
                Q(cliente__ndoc__icontains=wcliente) |
                Q(cliente_phone__icontains=wcliente) |
                Q(cliente_phone2__icontains=wcliente)
            )
            qfilters.add(
                sentence, qfilters.connector,
            )
        if winvoice != '':
            qfilters.add(
                Q(order=winvoice),
                qfilters.connector,
            )
        # added 2019.05.13
        if wleyenda != '':
            qfilters.add(
                Q(sub_tipo_invoice__id=wleyenda), qfilters.connector,
            )

        wqueryset = (
            qs_invoice_parent().filter(qfilters)
            .annotate(ndetails=Count('invoicedetails'))
            .order_by('-fhregistro')
        )
        outcome = wqueryset
    except Exception as e:
        error = str(e)
    return [outcome, error]


def get_list_backoffice(**kwargs):
    """ get orders list from backoffice module """
    outcome, error = [], None
    try:
        date_from = datetime.strptime(
            kwargs.get("date_from"), DATE_FORMAT)
        date_to = datetime.strptime(
            kwargs.get("date_to"), DATE_FORMAT)

        qrange = Q()
        qfilter = Q(estado_id=1,)
        if kwargs.get("date_filter"):
            qrange.add(
                Q(fentrega__range=[date_from, date_to]),
                qrange.connector
            )
        else:
            qrange.add(Q(fregistro__range=[date_from, date_to]),
                       qrange.connector)

        if kwargs.get("customer"):
            sentence = (
                Q(cliente__razon_social__icontains=kwargs.get("customer")) |
                Q(cliente__ndoc__icontains=kwargs.get("customer")) |
                Q(cliente_phone__icontains=kwargs.get("customer")) |
                Q(cliente_phone2__icontains=kwargs.get("customer"))
            )
            qfilter.add(sentence, qfilter.connector)

        if kwargs.get("order"):
            qfilter.add(
                Q(order__iexact=kwargs.get("order")), qfilter.connector)

        if kwargs.get("sub_tipo_invoice"):
            qfilter.add(
                Q(sub_tipo_invoice__id=kwargs.get("sub_tipo_invoice")),
                qfilter.connector)

        qs = (
            qs_invoice_parent().filter(qfilter)
            .annotate(
                fentrega=Case(
                    When(fecha_entrega_reprogramada__isnull=False,
                         then=F('fecha_entrega_reprogramada')),
                    default=F('fecha_entrega')),)
            .filter(qrange)
            .order_by("-fhregistro")
        )

        outcome = qs
    except Exception as e:
        error = str(e)
    return [outcome, error]


def get_new_correlative():
    """ get new order number """
    qsorder = mdls.InvoiceParent.objects.all() \
        .annotate(last_order=Cast('order', IntegerField())) \
        .values("last_order") \
        .order_by("-last_order")[:1]
    item = 0
    if qsorder.count() > 0:
        item = qsorder.first().get("last_order")
    item = int(item + 1)
    return "{:06}".format(item)


def get_styles():
    """ get styles for order report """
    return {
        "fullBorder": Border(
            left=Side(border_style='thin', color='00000000'),
            right=Side(border_style='thin', color='00000000'),
            top=Side(border_style='thin', color='00000000'),
            bottom=Side(border_style='thin', color='00000000')
        ),
    }


def get_style_footer_label():
    """ get xls style for order report """
    return NamedStyle(
        name="footer_style_label",
        font=Font(size=10, bold=True),
        border=get_styles().get("fullBorder"),
        fill=PatternFill(
            fill_type='solid',
            start_color='ffff66',
        ),
    )


def get_style_footer_value():
    """ get xls style for order report """
    return NamedStyle(
        name="footer_style_value",
        font=Font(size=10, bold=True),
        border=get_styles().get("fullBorder"),
        fill=PatternFill(
            fill_type='solid',
            start_color='ffffff',
        ),
        alignment=Alignment(horizontal="right"),
    )


def get_style_detail_header():
    """ get xls style for order report """
    return NamedStyle(
        name="detail_style_header",
        font=Font(size=10, bold=True,),
        fill=PatternFill(
            fill_type='solid',
            start_color='ffff66',
        ),
        alignment=Alignment(horizontal="center", vertical="center",),
        border=Border(
            top=Side(border_style='thin', color='00000000'),
            bottom=Side(border_style='thin', color='00000000'),
        ),
    )


def get_style_detail():
    """ get xls style for order report """
    return {
        "text": NamedStyle(
            name="detail_style_text",
            alignment=Alignment(horizontal="left",),
            font=Font(size=10),
            fill=PatternFill(
                fill_type='solid',
                start_color='ffffff',
            )),
        "number": NamedStyle(
            name="detail_style_number",
            alignment=Alignment(horizontal="right",),
            font=Font(size=10),
            fill=PatternFill(
                fill_type='solid',
                start_color='ffffff',
            )),
        "index": NamedStyle(
            name="detail_style_index",
            alignment=Alignment(horizontal="center",),
            font=Font(size=10),
            fill=PatternFill(
                fill_type='solid',
                start_color='ffffff',
            )),
    }


def validate_invoice(**kwargs):
    """ validate invoice """
    status, outcome = False, None
    try:
        qfilters = Q(
            order__iexact=kwargs.get("order"),
            # tipo_doc=kwargs.get("tipo_doc"),
        )
        qs = mdls.InvoiceParent.objects.filter(qfilters)
        if qs.count() > 0:
            raise ValueError(
                'Error, nro. documento ya se encuentra registrado')
        status = True
    except Exception as e:
        outcome = str(e)
    return [status, outcome]


def get_xls_backoffice(request, data):
    """ generate backoffice xls report """
    result, error = None, None
    datetime_format = "%d/%m/%Y %H:%M:%S"
    tz_format = "%Y-%m-%d %H:%M:%S"
    date_format = "%d/%m/%Y"
    time_format = "%H:%M:%S"
    tzdate_format = "%Y-%m-%d"
    try:
        wfields = [
            {"value": "Nro. Orden", "field": "order", "bold": True, "w": 15, },
            # {"value": "Tipo", "field": "tipo_descrip", "bold": True, "w": 15, },
            {"value": "Fecha Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Hora Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": time_format, "w": 20, },
            {"value": "Fecha Emision", "field": "fecha_emision", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Cliente", "field": "cliente_nombre", "bold": True, "w": 40, },
            {"value": "Nro. Telefono", "field": "cliente_phone",
                "bold": True, "w": 20, },
            {"value": "Ciudad", "field": "ciudad_descrip", "bold": True, "w": 20, },
            {"value": "Pais", "field": "pais_descrip", "bold": True, "w": 20, },
            {"value": "Leyenda",
                "field": "sub_tipo_invoice_descrip", "bold": True, "w": 30, },
            {"value": "Tipo Seguimiento",
                "field": "tipo_seguimiento_descrip", "bold": True, "w": 30, },
            {"value": "Total", "field": "total", "bold": True, },
            {"value": "Forma Pago", "field": "forma_pago_descrip",
                "bold": True, "w": 15, },
            {"value": "Fch.Entrega", "field": "fecha_entrega", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Fch.Reprogramada", "field": "fecha_entrega_reprogramada", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            # {"value": "Vendedor", "field": "vendedor_nombre", "bold": True, "w": 30, },
            {"value": "Usuario", "field": "username", "bold": True, },
            {"value": "Fch.Modif.", "field": "fhmodificacion_format", "bold": True,
             "format_in": tz_format, "format_out": datetime_format, "w": 20, },
            {"value": "Usuario Modif.", "field": "username_modif", "bold": True, },
        ]
        wparameters = {
            "start_from": 4,
            "request": request,
            "defaultWidth": 12,
            "title": "Reporte General de BackOffice",
            "headers": wfields,
            "details": data,
        }
        objxls = ExportToExcel(**wparameters)
        outcome, error = objxls.generateFile()
        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": outcome.get("filepath"),
        }
    except Exception as e:
        error = str(e)
    return [result, error]


def get_xls_invoice(request, data):
    """ generate order report """
    result, error = None, None
    hash_format = "%Y%m%d%H%M%S%f"
    try:
        wfilename = datetime.now().strftime(hash_format)
        wtemplate = '{}others/template_invoice.xlsx'.format(
            settings.INTERNAL_PATH.get("static"),
        )
        wxlsfile = '{}invoice_report/xls_{}.xlsx'.format(
            settings.INTERNAL_PATH.get("media"), wfilename,
        )
        werror = Validators.ifDirectoryExists(os.path.dirname(wxlsfile))
        if werror:
            raise ValueError(str(werror))
        copyfile(wtemplate, wxlsfile)
        wb = load_workbook(wxlsfile)
        ws = wb.active

        # print values about parent
        wcells = [
            "E5", "E6", "E7", "E8", "E9",
            "K5", "K6", "K7",
        ]
        client = data.get("cliente")
        p_phones = '{} / {}'.format(
            data.get("cliente_phone"), data.get("cliente_phone2"),
        )
        wvalues = [
            data.get("order"),
            client.get("razon_social"),
            data.get("cliente_address"),
            data.get("cliente_country"),
            data.get("observaciones"),
            data.get("fecha_emision"),
            client.get("ndoc"),
            p_phones,
        ]

        for windex, wcell in enumerate(wcells):
            ws[wcell].value = wvalues[windex]
        # print values about details
        styl_detail = get_style_detail()
        style_det_head = get_style_detail_header()
        wrow = 13
        det = {
            "h": ["C", "J", "L", "M", "E", "F", "H"],
            "v": [
                {"field": "item", "style": styl_detail.get("index"), },
                {"field": "cantidad", "style": styl_detail.get("number"), },
                {"field": "subtotal", "style": styl_detail.get("number"), },
                {"field": "total", "style": styl_detail.get("number"), },
                {
                    "field": "article_descrip",
                    "style": styl_detail.get("text"),
                },
                {
                    "field": "size_descrip",
                    "style": styl_detail.get("text"),
                },
                {
                    "field": "color_descrip",
                    "style": styl_detail.get("text"),
                },
            ],
        }
        for wrange in ws["B12":"L12"]:
            for c in wrange:
                c.style = style_det_head
        # print details
        for x in data.get("invoicedetails_set"):
            for n, y in enumerate(det.get("h")):
                wcell = ws['{}{}'.format(y, wrow)]
                wcell.value = x.get(det.get("v")[n].get("field"))
                wcell.style = det.get("v")[n].get("style")
            wrow += 1
        # print total values
        wrow += 3
        footer = [
            {"label": "Total Sale", "value": data.get("total"), },
            {"label": "Shipping", "value": 0, },
            {"label": "Total Order", "value": data.get("total"), },
        ]
        styl_foo_value = get_style_footer_value()
        styl_foo_label = get_style_footer_label()
        for x in footer:
            ws['L{}'.format(wrow)].value = x.get("label")
            ws['M{}'.format(wrow)].value = x.get("value")
            ws['L{}'.format(wrow)].style = styl_foo_value
            ws['M{}'.format(wrow)].style = styl_foo_label
            wrow += 1

        wb.save(wxlsfile)
        wb.close()

        urlpath = request.build_absolute_uri('{}invoice_report/{}'.format(
            settings.MEDIA_URL, os.path.basename(wxlsfile),
        ))

        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": urlpath,
        }
    except Exception as e:
        error = str(e)
    return [result, error]


def get_xls_report(request, data):
    """ generate orders xls report """
    result, error = None, None
    datetime_format = "%d/%m/%Y %H:%M:%S"
    tz_format = "%Y-%m-%d %H:%M:%S"
    date_format = "%d/%m/%Y"
    time_format = "%H:%M:%S"
    tzdate_format = "%Y-%m-%d"
    try:
        wfields = [
            {"value": "Nro. Orden", "field": "order", "bold": True, "w": 15, },
            # {"value": "Tipo", "field": "tipo_descrip", "bold": True, "w": 15, },
            {"value": "Fecha Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": date_format, "w": 20, },
            {"value": "Hora Registro", "field": "fhregistro_format", "bold": True,
             "format_in": tz_format, "format_out": time_format, "w": 20, },
            {"value": "Fecha Emision", "field": "fecha_emision", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Cliente", "field": "cliente_nombre", "bold": True, "w": 40, },
            {"value": "Nro. Telefono", "field": "cliente_phone",
                "bold": True, "w": 20, },
            {"value": "Ciudad", "field": "ciudad_descrip", "bold": True, "w": 20, },
            {"value": "Pais", "field": "pais_descrip", "bold": True, "w": 20, },
            {"value": "Nro. Items", "field": "ndetails", "bold": True, },
            {"value": "Total", "field": "total", "bold": True, },
            {"value": "Forma Pago", "field": "forma_pago_descrip",
                "bold": True, "w": 15, },
            {"value": "Fch.Entrega", "field": "fecha_entrega", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            {"value": "Fch.Reprogramada", "field": "fecha_entrega_reprogramada", "bold": True,
             "format_in": tzdate_format, "format_out": date_format, "w": 20, },
            # {"value": "Vendedor", "field": "vendedor_nombre", "bold": True, "w": 30, },
            {"value": "Usuario", "field": "username", "bold": True, },
            {"value": "Fch.Modif.", "field": "fhmodificacion_format", "bold": True,
             "format_in": tz_format, "format_out": datetime_format, "w": 20, },
            {"value": "Usuario Modif.", "field": "username_modif", "bold": True, },
        ]
        wparameters = {
            "start_from": 4,
            "request": request,
            "defaultWidth": 12,
            "title": "Reporte General de Invoices",
            "headers": wfields,
            "details": data,
        }
        objxls = ExportToExcel(**wparameters)
        outcome, error = objxls.generateFile()
        result = {
            "message": "Reporte fue generado satisfactoriamente.",
            "urlpath": outcome.get("filepath"),
        }
    except Exception as e:
        error = str(e)
    return [result, error]
