
#! /usr/bin/env python
# -*- coding:utf-8 -*-

from rest_framework import serializers
from apps.invoice import models as m
from apps.catalogos import serializers as srlcat
from apps.productos import serializers as srlprod
from apps.common import serializers as srlcommon


class InvoiceEstadosSerializer(srlcommon.DynamicFieldsModelSerializer):
    """ serializer for InvoiceEstados model """
    estado = srlcat.EstadosSerializer(
        fields=['id', 'descripcion'], read_only=True,)

    class Meta:
        model = m.InvoiceEstados
        fields = '__all__'


class InvoiceDetailsSerializer(srlcommon.DynamicFieldsModelSerializer):
    """ serializer for InvoiceDetails model """
    article = srlprod.ArticulosSerializer(
        fields=[
            'id', 'descripcion', 'categoria',
        ],
        read_only=True,
    )
    size = srlcat.SizesSerializer(
        fields=[
            'id', 'descripcion',
        ],
        read_only=True,
    )
    color = srlcat.ColorSerializer(
        fields=[
            'id', 'descripcion',
        ],
        read_only=True,
    )
    size_descrip = serializers.SerializerMethodField()
    color_descrip = serializers.SerializerMethodField()
    article_descrip = serializers.SerializerMethodField()

    class Meta:
        model = m.InvoiceDetails
        fields = '__all__'

    def get_article_descrip(self, obj):
        """ get serializer value of article_descrip """
        if hasattr(obj.article, 'descripcion'):
            return obj.article.descripcion
        return ''

    def get_color_descrip(self, obj):
        """ get serializer value of color_descrip """
        if hasattr(obj.color, 'descripcion'):
            return obj.color.descripcion
        return ''

    def get_size_descrip(self, obj):
        """ get serializer value of size_descrip """
        if hasattr(obj.size, 'descripcion'):
            return obj.size.descripcion
        return ''


class InvoiceParentSerializer(srlcommon.DynamicFieldsModelSerializer):
    """ serializer for InvoiceParent model """

    estado = srlcat.EstadosSerializer(
        fields=[
            'id', 'descripcion', 'estado',
        ],
        read_only=True,
    )
    sub_tipo_invoice = InvoiceEstadosSerializer(
        fields=[
            'id', 'descripcion', 'estado',
        ],
        read_only=True,
    )
    ndetails = serializers.IntegerField()
    cliente_nombre = serializers.SerializerMethodField()

    cliente = srlcat.ClientesSerializer(
        fields=[
            'id', 'razon_social',
            'direccion',
            'ndoc', 'tdoc',
            "telefono",
            "celular", "contacto",
        ],
        read_only=True,
    )
    invoicedetails_set = InvoiceDetailsSerializer(
        read_only=True,
        many=True,
        fields=[
            "id", "item", "article",
            "article", "article_descrip",
            "size", "size_descrip",
            "color", "color_descrip",
            "cantidad", "subtotal",
            "igv", "total",
        ],
    )

    tipo_invoice = srlcat.TipoInvoiceSerializer(
        fields=[
            'id', 'codigo', 'descripcion',
        ],
        read_only=True,
    )
    forma_pago = srlcat.FormaPagoSerializer(
        fields=[
            'id', 'codigo', 'descripcion',
        ],
        read_only=True,
    )
    tipo_seguimiento = srlcat.CombosSerializer(
        fields=[
            'id', 'descripcion',
        ],
        read_only=True,
    )
    forma_pago_descrip = serializers.SerializerMethodField()
    tipo_descrip = serializers.SerializerMethodField()
    vendedor_nombre = serializers.SerializerMethodField()
    tipo_seguimiento_descrip = serializers.SerializerMethodField()
    sub_tipo_invoice_descrip = serializers.SerializerMethodField()
    pais_descrip = serializers.SerializerMethodField()
    ciudad_descrip = serializers.SerializerMethodField()
    vendedor = srlcat.VendedoresSerializer(
        fields=['id', 'nombres', ],
        read_only=True,
    )

    class Meta:
        model = m.InvoiceParent
        fields = '__all__'

    def get_tipo_descrip(self, obj):
        """ get serializer value of tipo_descrip """
        if hasattr(obj.tipo_invoice, 'descripcion'):
            return obj.tipo_invoice.descripcion
        return ''

    def get_forma_pago_descrip(self, obj):
        """ get serializer value of forma_pago_descrip """
        if hasattr(obj.forma_pago, 'descripcion'):
            return obj.forma_pago.descripcion
        return ''

    def get_cliente_nombre(self, obj):
        """ get serializer value of cliente_nombre """
        if hasattr(obj.cliente, 'razon_social'):
            return obj.cliente.razon_social
        return ''

    def get_vendedor_nombre(self, obj):
        """ get serializer value of vendedor_nombre """
        if hasattr(obj.vendedor, 'nombres'):
            return obj.vendedor.nombres
        return ''

    def get_tipo_seguimiento_descrip(self, obj):
        """ get serializer value of tipo_seguimiento_descrip """
        if hasattr(obj.tipo_seguimiento, 'descripcion'):
            return obj.tipo_seguimiento.descripcion
        return ''

    def get_sub_tipo_invoice_descrip(self, obj):
        """ get serializer value of sub_tipo_invoice_descrip """
        if hasattr(obj.sub_tipo_invoice, 'descripcion'):
            return obj.sub_tipo_invoice.descripcion
        return ''

    def get_pais_descrip(self, obj):
        """ get serializer value of pais_descrip """
        if hasattr(obj.cliente, 'pais'):
            if hasattr(obj.cliente.pais, "descripcion"):
                return obj.cliente.pais.descripcion
        return ''

    def get_ciudad_descrip(self, obj):
        """ get serializer value of ciudad_descrip """
        if hasattr(obj.cliente, 'ciudad'):
            return obj.cliente.ciudad
        return ''
