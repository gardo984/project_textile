from django.contrib import admin
from apps.invoice import models as mdls

# Register your models here.


class InvoiceEstadosAdmin(admin.ModelAdmin):
    list_display = [
        "id", "descripcion",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion", ]
    fields = [
        "id", "descripcion",
        "fregistro", "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion", "fregistro"]
    search_fields = [
        "id", "descripcion", "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class InvoiceParentAdmin(admin.ModelAdmin):
    list_display = [
        "id", "razon_social", "tdoc",  "ndoc",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "razon_social", "tdoc", ]
    fields = [
        "id", "razon_social", "direccion",
        "tdoc", "ndoc",
        "telefono", "celular", "email",
        "contacto", "observaciones",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "razon_social", "ndoc",
        "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if not obj.fhregistro is None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if not obj.fhmodificacion is None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"

wmodels = [
    # mdls.InvoiceParent,
    mdls.InvoiceEstados,
]
winterfaces = [
    # InvoiceParentAdmin,
    InvoiceEstadosAdmin,
]

for windex, witem in enumerate(wmodels):
    admin.site.register(wmodels[windex], winterfaces[windex])
