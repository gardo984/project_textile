from django.conf.urls import url
from apps.invoice import views as v

app_name = 'module_invoice'

urlpatterns = [
    url(r'^$', v.ViewInvoice.as_view(), name='main'),
    url(r'^backoffice/$',
        v.ViewBackOffice.as_view(), name='backoffice'),
]
