
import json
from django.conf import settings
from django.shortcuts import render, redirect
from django.views import View
from django.http import JsonResponse
from django.db.models import (
    Q, F, Prefetch,
)
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from apps.invoice import (
    models as mdls,
    serializers as srlz,
    forms as frm,
)
from apps.catalogos import (
    models as mdlcat,
)
from apps.invoice import utils as utl

# Create your views here.


DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'
DEFAULT_RESPONSE = {
    "ok": 1,
    "result": 'Process has been processed successfuly.',
    "status": 200,
}
BRAND = settings.PROJECT_COMPANY


@method_decorator(login_required, name='dispatch')
class ViewInvoice(View):
    """ class based view for invoice model """
    template_name = 'invoices/invoice.html'
    template_title = 'Ordenes'
    template_header = '{} | {}'.format(
        BRAND, template_title
    )

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'invoice.display_invoiceparent', login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        """ handle get method requests """
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    def post(self, request):
        """ handle post method requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "add_invoice":
                    return self.add_invoice(request)
                if wtype == "get_invoices":
                    return self.get_invoices(request)
                if wtype == "get_invoice_item":
                    return self.get_invoice_item(request)
                if wtype == "remove_invoice":
                    return self.remove_invoice(request)
                if wtype == "update_invoice":
                    return self.update_invoice(request)
                if wtype == "get_xls_invoice":
                    return self.get_xls_invoice(request)
                if wtype == "get_xls_report":
                    return self.get_invoices_report(request)
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.display_invoiceparent', raise_exception=True),)
    def get_invoices(self, request):
        """ get orders list """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            select_fields = utl.get_orders_fields()
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })

            outcome, err = utl.get_invoices_list(**wdata)
            if err:
                raise ValueError(err)

            page["total"] = outcome.count()
            srlzdata = srlz.InvoiceParentSerializer(
                outcome[wfrom:wto], fields=select_fields, many=True
            ).data
            wresponse['result'] = {
                "rows": srlzdata,
                "page": page,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.xlsreport_invoiceparent',
        raise_exception=True),)
    def get_invoices_report(self, request):
        """ get general orders report """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            select_fields = utl.get_orders_xls_fields()
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })
            outcome, err = utl.get_invoices_list(**wdata)
            if err:
                raise ValueError(err)

            srlzdata = srlz.InvoiceParentSerializer(
                outcome, fields=select_fields, many=True,
            ).data
            wreport, werror = utl.get_xls_report(request, srlzdata)
            if werror:
                raise ValueError(str(werror))
            wresponse["result"] = wreport
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.xlsinvoice_invoiceparent', raise_exception=True),)
    def get_xls_invoice(self, request):
        """ get order report """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            winvoice = wdata.get('uid') or ''
            if winvoice == '':
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))

            qfilters = Q(id=int(winvoice))
            wqueryset = utl.qs_invoice_parent().filter(qfilters)
            if not wqueryset.exists:
                wresponse['status'] = 400
                raise ValueError(str('Error, invalid Invoice ID specified.'))

            p_fields = utl.get_order_item_fields()
            wsource = srlz.InvoiceParentSerializer(
                wqueryset.get(), fields=p_fields,
            ).data
            wreport, werror = utl.get_xls_invoice(request, wsource)
            if werror:
                raise ValueError(str(werror))
            wresponse['result'] = wreport
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    def get_invoice_item(self, request):
        """ get invoice record """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            winvoice = wdata.get('id') or ''
            if winvoice == '':
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilters = Q(id=int(winvoice))

            wqueryset = utl.qs_invoice_parent().filter(qfilters)
            fields = utl.get_order_item_fields()
            if not wqueryset.exists:
                wresponse['status'] = 400
                raise ValueError(str('Error, invalid Invoice ID specified.'))
            woutcome = srlz.InvoiceParentSerializer(
                wqueryset.get(), fields=fields,
            ).data
            for item in woutcome.get("invoicedetails_set"):
                item["check"], item["visible"] = False, True
            wresponse['result'] = woutcome

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(
            wresponse, status=wresponse.get("status")
        )

    @method_decorator(permission_required(
        'invoice.add_invoiceparent', raise_exception=True),)
    def add_invoice(self, request):
        """ handle order registration """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wdata["order"] = utl.get_new_correlative()

            form = frm.MFInvoiceParent(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")
            # parent layer validation
            if not form.is_valid():
                werror = form.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 422
                raise ValueError(werr_msg)

            # details layer validation
            wdetails = wdata.get("articulos") or []
            result, error = self.__validate_details(wdetails)
            if error:
                raise ValueError(str(error))

            wparameters = form.clean()
            status, err = utl.validate_invoice(**wparameters)
            if err:
                wresponse["status"] = 422
                raise ValueError(str(err))

            wparameters["username"] = request.user
            objinvoice = mdls.InvoiceParent(**wparameters)
            objinvoice.save()
            error = self.__update_details(objinvoice, result)
            if error:
                raise ValueError(str(error))

            confirmation = u'{}, {}'.format(
                u'Orden fue registrado satisfactoriamente',
                u'Nro. {}'.format(objinvoice.order)
            )
            wresponse['result'] = {
                "id": objinvoice.pk, "message": confirmation,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    def __update_details(self, invoice, details):
        """ handle order details registration """
        error = None
        try:
            mdls.InvoiceDetails.objects.filter(invoice=invoice).delete()
            for item in details:
                c = mdls.InvoiceDetails(**item)
                c.invoice = invoice
                c.save()
        except Exception as e:
            error = str(e)
        return error

    def __validate_details(self, details):
        """ handle invoice details validations """
        data, error,  = [], None
        try:
            for item in details:
                wparameters = item.copy()
                wparameters["article"] = item.get("article").get("id")
                wparameters["size"] = item.get("size").get("id")
                wparameters["color"] = item.get("color").get("id")
                form = frm.MFInvoiceDetails(wparameters)
                if not form.is_valid():
                    werror = form.get_error_first()
                    raise ValueError(werror)
                result = form.clean()
                data.append(result)
        except Exception as e:
            error = str(e)
        return [data, error]

    @method_decorator(permission_required(
        'invoice.change_invoiceparent', raise_exception=True),)
    def update_invoice(self, request):
        """ handle order updating process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wid = wdata.get("id") or ''
            if wid == '':
                raise ValueError('Error, id es valor requerido.')

            objinvoice = mdls.InvoiceParent.objects.filter(id=int(wid))
            if not objinvoice.exists():
                wresponse['status'] = 400
                raise ValueError("Error, Invalid Invoice ID specified.")

            # parent layer validation
            form = frm.MFInvoiceParent(wdata)
            if not form.is_valid():
                werror = form.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)
            wparameters = form.clean()
            # details layer validation
            wdetails = wdata.get("articulos") or []
            result, error = self.__validate_details(wdetails)
            if error:
                raise ValueError(str(error))
            wparameters["fhmodificacion"] = timezone.now()
            wparameters["username_modif"] = request.user
            objinvoice.update(**wparameters)
            error = self.__update_details(
                objinvoice.get(), result
            )
            if error:
                raise ValueError(str(error))

            wresponse['result'] = {
                "id": objinvoice.get().id,
                "message": "Datos fueron procesados satisfactoriamente.",
            }

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.delete_invoiceparent',
        raise_exception=True),)
    def remove_invoice(self, request):
        """ handle order removal process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widinvoice = wdata.get("item").get("id") or 0
            mdls.InvoiceParent.objects.get(id=int(widinvoice)).delete()
            wresponse['result'] = {
                "message": "Orden fue eliminado satisfactoriamente.",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ViewBackOffice(View):
    """ class based view backoffice module """
    template_name = 'invoices/backoffice.html'
    template_title = 'Backoffice'
    template_header = '{} | {}'.format(
        BRAND, template_title
    )

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'invoice.display_backoffice',
        login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        """ handle get method requests """
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'invoice.display_backoffice', raise_exception=True,),)
    def post(self, request):
        """ handle post method requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_list_items":
                    return self.get_list_items(request)
                if wtype == "get_item_values":
                    return self.get_item_values(request)
                if wtype == "update_order":
                    return self.update_order(request)
                if wtype == "get_xls_report":
                    return self.get_backoffice_report(request)

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    def get_list_items(self, request):
        """ get order's details """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            wto = 10
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            select_fields = utl.get_backoffice_fields()
            wdata.update({
                "select_fields": select_fields,
            })
            outcome, err = utl.get_list_backoffice(**wdata)
            if err:
                raise ValueError(err)

            srlzdata = srlz.InvoiceParentSerializer(
                outcome[wfrom:wto], fields=select_fields, many=True,
            ).data

            page["total"] = outcome.count()
            wresponse['result'] = {
                "rows": srlzdata,
                "page": page,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.change_backoffice', raise_exception=True),)
    def get_item_values(self, request):
        """ get order record - backoffice """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            winvoice = wdata.get('id') or ''
            if winvoice == '':
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            qfilters = Q(id=int(winvoice))

            wqueryset = (
                utl.qs_invoice_parent().filter(qfilters)
            )
            fields = utl.get_backoffice_item_fields()
            if not wqueryset.exists:
                wresponse['status'] = 400
                raise ValueError(str('Error, invalid Order ID specified.'))
            woutcome = srlz.InvoiceParentSerializer(
                wqueryset.get(), fields=fields,
            ).data
            for item in woutcome.get("invoicedetails_set"):
                item["check"], item["visible"] = False, True
            wresponse['result'] = woutcome

        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.change_backoffice', raise_exception=True),)
    def update_order(self, request):
        """ handle order updating process - backoffice """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wid = wdata.get("id") or ''
            if wid == '':
                raise ValueError('Error, id es valor requerido.')

            objorder = mdls.InvoiceParent.objects.filter(id=int(wid))
            if not objorder.exists():
                wresponse['status'] = 400
                raise ValueError("Error, Invalid Order ID specified.")

            # parent layer validation
            form = frm.MFBackOffice(wdata)
            if not form.is_valid():
                werror = form.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)
            wparameters = form.clean()

            wparameters["fhmodificacion"] = timezone.now()
            wparameters["username_modif"] = request.user
            objorder.update(**wparameters)
            wresponse['result'] = {
                "id": objorder.get().id,
                "message": "Datos fueron procesados satisfactoriamente.",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'invoice.xlsreport_backoffice', raise_exception=True),)
    def get_backoffice_report(self, request):
        """ backoffice report generation """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            select_fields = utl.get_backoffice_xls_fields()
            wdata.update({
                "select_fields": select_fields,
            })
            outcome, err = utl.get_list_backoffice(**wdata)
            if err:
                raise ValueError(err)

            srlzdata = srlz.InvoiceParentSerializer(
                outcome, fields=select_fields, many=True,
            ).data
            wreport, werror = utl.get_xls_backoffice(request, srlzdata)
            if werror:
                raise ValueError(str(werror))
            wresponse['result'] = wreport
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))
