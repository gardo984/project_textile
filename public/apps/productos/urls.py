from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.productos import views as v

app_name = 'module_products'

urlpatterns = [
    url(r'^category/$', v.ViewCategory.as_view(), name='category'),
    url(r'^articles/$', v.ViewArticle.as_view(), name='articles'),
]
