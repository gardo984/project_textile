from django.contrib import admin
from apps.productos import models as mdls
from apps.productos import forms as frm
# Register your models here.


class CategoriaAdmin(admin.ModelAdmin):
    """ admin interface for category model """
    list_display_links = ["id", "descripcion"]
    list_display = [
        "id", "descripcion", "_format_fhregistro",
        "_format_fhmodificacion",
        "estado",
    ]
    fields = [
        "id", "descripcion",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "descripcion", "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if obj.fhregistro is not None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if obj.fhmodificacion is not None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


class ArticulosAdmin(admin.ModelAdmin):
    """ admin interface for article model """

    form = frm.MFArticles
    list_display = [
        "id", "descripcion", "categoria", "precio",
        "_format_fhregistro", "_format_fhmodificacion",
        "estado",
    ]
    list_display_links = ["id", "descripcion"]
    fields = [
        "id", "descripcion", "categoria",
        "precio",
        "fhregistro", "fhmodificacion",
        "estado",
    ]
    readonly_fields = ["id", "fhregistro", "fhmodificacion"]
    search_fields = [
        "id", "descripcion",
        "categoria__descripcion", "estado__descripcion",
    ]
    ordering = ["-fhregistro"]
    list_per_page = 20
    empty_value_display = ""

    def _format_fhregistro(self, obj):
        wvalue = None
        if obj.fhregistro is not None:
            wvalue = obj.fhregistro.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhregistro.short_description = "Fecha Creacion"
    _format_fhregistro.admin_order_field = "-fhregistro"

    def _format_fhmodificacion(self, obj):
        wvalue = None
        if obj.fhmodificacion is not None:
            wvalue = obj.fhmodificacion.strftime("%Y-%m-%d %H:%M:%S")
        return wvalue
    _format_fhmodificacion.short_description = "Fecha Modificacion"
    _format_fhmodificacion.admin_order_field = "-fhmodificacion"


MODELS = [
    mdls.Categoria, mdls.Articulos,
]
INTERFACES = [
    CategoriaAdmin, ArticulosAdmin
]

for windex, witem in enumerate(MODELS):
    admin.site.register(MODELS[windex], INTERFACES[windex])
