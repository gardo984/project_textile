import json
from django.conf import settings
from django.shortcuts import render
from django.views import View
from django.http import JsonResponse
from django.db.models import (
    Q,
)
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required, permission_required,
)
from django.core.exceptions import PermissionDenied
from apps.productos import (
    models as mdls,
    serializers as srlz,
)
from apps.catalogos import models as mdlcat
# Create your views here.

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'


DEFAULT_RESPONSE = {
    "ok": 1,
    "result": 'Process has been processed successfuly.',
    "status": 200,
}
BRAND = settings.PROJECT_COMPANY


@method_decorator(login_required, name='dispatch')
class ViewCategory(View):
    """ class based view for Category model """
    template_name = 'productos/products_category.html'
    template_title = ' de Categorias'
    template_header = '{} | {}'.format(
        BRAND, template_title
    )

    def dispatch(self, *args, **kwargs):
        return super(ViewCategory, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'productos.add_categoria',
        login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        """ handle get method requests """
        wresponse = DEFAULT_RESPONSE.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'productos.add_categoria', raise_exception=True),)
    def post(self, request):
        """ handle post method requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_products_categories":
                    return self.getCategories(request)
                if wtype == "remove_products_category":
                    return self.removeCategory(request)
                if wtype == "update_products_category":
                    return self.updateCategory(request)
                if wtype == "add_products_category":
                    return self.addCategory(request)
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.add_categoria', raise_exception=True),)
    def getCategories(self, request):
        """ get category list """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wcategoria = wdata.get('categoria') or ''
            westado = wdata.get('estado') or ''
            qfilters = Q(id__isnull=False)
            if wcategoria != '':
                qfilters.add(
                    Q(descripcion__icontains=wcategoria),
                    qfilters.connector,
                )
            if westado != '':
                qfilters.add(
                    Q(estado__id=westado),
                    qfilters.connector,
                )

            wqueryset = (
                mdls.Categoria.objects.filter(qfilters)
                .select_related("estado")
            )
            woutcome = srlz.CategoriaSerializer(
                wqueryset, many=True
            ).data
            wresponse['result'] = woutcome
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.add_categoria', raise_exception=True),)
    def addCategory(self, request):
        """ handle registration category process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wcategoria = wdata.get("categoria")
            westado = wdata.get("estado")
            objcategory = mdls.Categoria(
                descripcion=wcategoria,
                estado=mdlcat.Estados.objects.get(id=westado)
            )
            objcategory.username = request.user
            objcategory.save()
            wresponse['result'] = {
                "id": objcategory.pk,
                "message": "Categoria fue registrada satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.change_categoria', raise_exception=True),)
    def updateCategory(self, request):
        """ handle update category process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widcategoria = wdata.get("id") or 0
            objcategory = mdls.Categoria.objects.get(id=widcategoria)
            objcategory.descripcion = wdata.get("categoria")
            objcategory.estado = mdlcat.Estados.objects.get(
                id=int(wdata.get("estado")))
            objcategory.save()
            wresponse['result'] = {
                "id": objcategory.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.delete_categoria', raise_exception=True),)
    def removeCategory(self, request):
        """ handle removal category process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widcategoria = wdata.get("item").get("id") or 0
            query = mdls.Categoria.objects.filter(
                id=int(widcategoria)
            )
            if query.exists():
                query.update(
                    fhmodificacion=timezone.now(),
                    username_modif=request.user,
                    estado=mdlcat.Estados.Flags.NOT_ACTIVE,
                )
            wresponse['result'] = {
                "message": "Categoria fue anulada satisfactoriamente.",
            }
            return JsonResponse(wresponse, status=wresponse.get("status"))
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return JsonResponse(wresponse, status=wresponse.get("status"))


@method_decorator(login_required, name='dispatch')
class ViewArticle(View):
    """ class based view for Article model """
    template_name = 'productos/products_articles.html'
    template_title = 'de Modelos'
    template_header = '{} | {}'.format(
        BRAND, template_title
    )

    def dispatch(self, *args, **kwargs):
        return super(ViewArticle, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'productos.add_articulos',
        login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'productos.add_articulos', raise_exception=True
    ),)
    def post(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "get_products_articles":
                    return self.getArticles(request)
                if wtype == "remove_products_article":
                    return self.removeArticle(request)
                if wtype == "update_products_article":
                    return self.updateArticle(request)
                if wtype == "add_products_article":
                    return self.addArticle(request)
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e) is PermissionDenied:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.add_articulos', raise_exception=True),)
    def getArticles(self, request):
        """ get article list """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wcategoria = wdata.get('categoria') or ''
            warticulo = wdata.get('articulo') or ''
            westado = wdata.get('estado') or ''
            qfilters = Q(id__isnull=False)
            if wcategoria != '':
                qfilters.add(
                    Q(categoria__id=wcategoria),
                    qfilters.connector,
                )
            if warticulo != '':
                qfilters.add(
                    Q(descripcion__icontains=warticulo),
                    qfilters.connector,
                )
            if westado != '':
                qfilters.add(
                    Q(estado__id=westado),
                    qfilters.connector,
                )

            wqueryset = mdls.Articulos.objects.filter(qfilters) \
                .select_related("estado") \
                .select_related("categoria__estado")

            woutcome = srlz.ArticulosSerializer(
                wqueryset, many=True
            ).data
            wresponse['result'] = woutcome
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.add_articulos', raise_exception=True
    ),)
    def addArticle(self, request):
        """ handle registration article process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            warticulo = wdata.get("articulo")
            wcategoria = wdata.get("categoria")
            westado = wdata.get("estado")
            objcategory = None
            if wcategoria is not None:
                objcategory = mdls.Categoria.objects.get(id=wcategoria)

            objarticle = mdls.Articulos(
                descripcion=warticulo,
                categoria=objcategory,
                estado=mdlcat.Estados.objects.get(id=westado),
            )
            objarticle.username = request.user
            objarticle.save()
            wresponse['result'] = {
                "id": objarticle.pk,
                "message": "Articulo fue registrada satisfactoriamente",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.change_articulos', raise_exception=True),)
    def updateArticle(self, request):
        """ handle update article process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widarticulo = wdata.get("id") or 0
            objarticle = mdls.Articulos.objects.get(id=widarticulo)
            objarticle.descripcion = wdata.get("articulo")
            if wdata.get("categoria") is not None:
                objarticle.categoria = mdls.Categoria.objects.get(
                    id=int(wdata.get("categoria"))
                )
            else:
                objarticle.categoria = None

            objarticle.estado = mdlcat.Estados.objects.get(
                id=int(wdata.get("estado")))
            objarticle.username_modif = request.user
            objarticle.save()
            wresponse['result'] = {
                "id": objarticle.pk,
                "message": "Cambios fueron actualizados satisfactoriamente",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    @method_decorator(permission_required(
        'productos.delete_articulos', raise_exception=True),)
    def removeArticle(self, request):
        """ handle removal article process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widarticulo = wdata.get("item").get("id") or 0
            query = mdls.Articulos.objects.filter(
                id=int(widarticulo)
            )
            if query.exists():
                query.update(
                    fhmodificacion=timezone.now(),
                    username_modif=request.user,
                    estado=mdlcat.Estados.Flags.NOT_ACTIVE,
                )
            wresponse['result'] = {
                "message": "Articulo fue anulada satisfactoriamente.",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))
