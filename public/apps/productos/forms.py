import json
from apps.productos import models as mdls
from django import forms


class MFArticles(forms.ModelForm):
    """ web form for article admin model """
    DEFAULT_MSG_REQUIRED = 'Este campo es requerido'
    DEFAULT_MSG_CHOICE = 'Ingrese una opcion valida, valor seleccionado no paso validacion'
    NO_REQUIRED = [
        "categoria",
    ]

    class Meta:
        model = mdls.Articulos
        fields = '__all__'
        exclude = [

        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for wfield in self.NO_REQUIRED:
            self.fields[wfield].required = False

        for wfield in self.fields.keys():
            lst_errors = self.fields[wfield].error_messages
            if lst_errors.get("required"):
                lst_errors["required"] = self.DEFAULT_MSG_REQUIRED
            if lst_errors.get("invalid_choice"):
                lst_errors["invalid_choice"] = self.DEFAULT_MSG_CHOICE

    def clean(self):
        cleaned_data = super().clean()
        for item in self.fields.keys():
            if isinstance(cleaned_data.get(item), str):
                cleaned_data[item] = cleaned_data.get(item).upper()
        return cleaned_data

    def get_error_first(self):
        wresult = {}
        if self.errors:
            wlist_json = json.loads(self.errors.as_json())
            wkey = list(wlist_json.keys())[0]
            wvalues = wlist_json.get(wkey)[0].get("message")
            wresult["field"] = wkey.lower()
            wresult["message"] = wvalues.lower()
        return wresult
