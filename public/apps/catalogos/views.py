import os
import json
from django.shortcuts import render
from django.views import View
from django.conf import settings
from django.http import JsonResponse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import (
    login_required,
    permission_required,
)
from django.core.exceptions import PermissionDenied
from apps.catalogos import (
    models as mdls,
    serializers as srlz,
    forms as frm,
    utils as utlcat,
)
from apps.common import utils as utl


# Create your views here.

DEFAULT_FORBIDDEN_PAGE = '/login/forbidden'

DEFAULT_RESPONSE = {
    "ok": 1,
    "result": 'Process has been processed successfuly.',
    "status": 200,
}
BRAND = settings.PROJECT_COMPANY


@method_decorator(login_required, name='dispatch')
class ViewCustomer(View):
    """ Class based view for Customer model """
    template_name = 'catalogos/catalogos_clientes.html'
    template_title = 'Clientes'
    template_header = '{} | {}'.format(BRAND, template_title)

    def dispatch(self, *args, **kwargs):
        return super(ViewCustomer, self).dispatch(*args, **kwargs)

    @method_decorator(permission_required(
        'catalogos.display_clientes',
        login_url=DEFAULT_FORBIDDEN_PAGE,),)
    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        header = self.template_header
        title = self.template_title
        return render(request, self.template_name, locals())

    @method_decorator(permission_required(
        'catalogos.display_clientes', raise_exception=True),)
    def post(self, request):
        """ handle post method requests """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            if not request.is_ajax():
                wresponse['status'] = 400
                raise ValueError(str('Bad Request'))
            if wdata.get("wtype"):
                wtype = wdata.get("wtype")
                if wtype == "add_catalogos_customer":
                    return self.addCustomer(request)
                if wtype == "get_catalogos_customers":
                    return self.getCustomers(request)
                if wtype == "remove_catalogos_customer":
                    return self.removeCustomer(request)
                if wtype == "update_catalogos_customer":
                    return self.updateCustomer(request)
                if wtype == "get_xls_report":
                    return self.getCustomerReport(request)
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if isinstance(e, PermissionDenied) is True:
                wresponse['status'], wresponse[
                    'result'] = 403, 'Permission Denied'
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(
            wresponse, status=wresponse.get("status")
        )

    @method_decorator(permission_required(
        'catalogos.display_clientes', raise_exception=True),)
    def getCustomers(self, request):
        """ get customer list """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))

            page = utl.Validators.getPageParameters()
            wfrom, wto = 0, page.get("size")
            if wdata.get("npage"):
                wto = page.get("size") * int(wdata.get("npage"))
                wfrom = wto - page.get("size")

            select_fields = [
                "id", "fhregistro", "fhmodificacion", "razon_social",
                "direccion", "ndoc", "telefono", "celular", "email",
                "observaciones", "estado", "tdoc",
                "contacto", "username", "username_modif",
                "ciudad", "pais", "tipificacion", "leyenda",
                "contactado", "estado_descripcion",
                "pais_descripcion", "tdoc_descripcion",
                "fhregistro_format", "fhmodificacion_format",
                "contactado_descripcion", "tipificacion_descripcion",
            ]
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })
            outcome, err = utlcat.getListCustomers(**wdata)
            if err:
                raise ValueError(str(err))

            page["total"] = outcome.count()
            srlzdata = srlz.ClientesSerializer(
                outcome[wfrom:wto], fields=select_fields, many=True,
            ).data
            wresponse["result"] = {
                "rows": srlzdata,
                "page": page,
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(
            wresponse, status=wresponse.get("status")
        )

    @method_decorator(permission_required(
        'catalogos.add_clientes', raise_exception=True),)
    def addCustomer(self, request):
        """ handle customer registration process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wstatus, werror = self.ifCustomerExist(wdata)
            if werror:
                wresponse['status'] = 400
                raise ValueError(str(werror))

            form = frm.MFClientes(wdata)
            if not form.is_bound:
                wresponse['status'] = 400
                raise ValueError("Bad Request")

            if not form.is_valid():
                werror = form.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)

            wparameters = form.clean()
            objcliente = mdls.Clientes(**wparameters)
            objcliente.username = request.user
            objcliente.save()
            wresponse['result'] = {
                "id": objcliente.pk,
                "message": "Cliente fue registrado satisfactoriamente",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(wresponse, status=wresponse.get("status"))

    def ifCustomerExist(self, source):
        """ validate if customer exists """
        status, error = False, None
        try:
            item = source
            ndocument = (item.get("ndoc") or '').strip()
            if item.get("id"):
                outcome = mdls.Clientes.objects.filter(
                    ndoc__iexact=ndocument,
                ).exclude(id=item.get("id"))
            else:
                outcome = mdls.Clientes.objects.filter(
                    ndoc__iexact=ndocument,
                )

            if outcome.count() > 0:
                raise ValueError('Error, Cliente ya se encuentra registrado.')
        except Exception as e:
            status, error = True, str(e)
        return [status, error]

    @method_decorator(permission_required(
        'catalogos.change_clientes', raise_exception=True),)
    def updateCustomer(self, request):
        """ handle customer update process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            wid = wdata.get("id") or ''
            if wid == '':
                raise ValueError('Error, id es valor requerido.')

            objcliente = mdls.Clientes.objects.filter(id=int(wid))
            if not objcliente.exists():
                wresponse['status'] = 400
                raise ValueError("Error, Cliente no existe.")

            wstatus, werror = self.ifCustomerExist(wdata)
            if werror:
                wresponse['status'] = 400
                raise ValueError(str(werror))

            form = frm.MFClientes(wdata)
            if not form.is_valid():
                werror = form.get_error_first()
                werr_msg = 'Error, {} : {}.'.format(
                    werror.get("field"), werror.get("message"),
                )
                wresponse['status'] = 400
                raise ValueError(werr_msg)
            wparameters = form.clean()
            wparameters["fhmodificacion"] = timezone.now()
            wparameters["username_modif"] = request.user
            objcliente.update(**wparameters)
            wresponse['result'] = {
                "id": objcliente.get().id,
                "message": "Datos fueron procesados satisfactoriamente.",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(
            wresponse, status=wresponse.get("status")
        )

    @method_decorator(permission_required(
        'catalogos.delete_clientes', raise_exception=True),)
    def removeCustomer(self, request):
        """ handle customer removal process """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            widcustomer = wdata.get("item").get("id") or 0
            query = (
                mdls.Clientes.objects.filter(id=int(widcustomer))
            )
            if query.exists():
                query.update(
                    fhmodificacion=timezone.now(),
                    username_modif=request.user,
                    estado=mdls.Estados.Flags.NOT_ACTIVE,
                )
            wresponse['result'] = {
                "message": "Cliente fue eliminado satisfactoriamente.",
            }
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(
            wresponse, status=wresponse.get("status")
        )

    @method_decorator(permission_required(
        'catalogos.xlsreport_clientes', raise_exception=True),)
    def getCustomerReport(self, request):
        """ customer xls report generation """
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = json.loads(request.body.decode("utf8"))
            select_fields = [
                "id", "fhregistro", "fhmodificacion", "razon_social",
                "direccion", "ndoc", "telefono", "celular", "email",
                "observaciones", "estado", "tdoc",
                "contacto", "username", "username_modif",
                "ciudad", "pais", "tipificacion", "leyenda",
                "contactado", "estado_descripcion",
                "pais_descripcion", "tdoc_descripcion",
                "fhregistro_format", "fhmodificacion_format",
                "contactado_descripcion", "tipificacion_descripcion",
            ]
            wdata.update({
                "user": request.user, "select_fields": select_fields,
            })
            outcome, err = utlcat.getListCustomers(**wdata)
            if err:
                raise ValueError(str(err))

            srlzdata = srlz.ClientesSerializer(
                outcome, fields=select_fields, many=True,
            ).data
            wreport, werror = utlcat.get_xls_report(request, srlzdata)
            if werror:
                raise ValueError(str(werror))
            wresponse["result"] = wreport
        except Exception as e:
            wresponse['ok'] = 0
            wresponse['result'] = str(e)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return JsonResponse(
            wresponse, status=wresponse.get("status")
        )
