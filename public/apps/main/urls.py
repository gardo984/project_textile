from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from apps.main import views as v

app_name = 'module_main'

urlpatterns = [
    url(r'^$', v.MainView.as_view(), name='navigator'),
]
