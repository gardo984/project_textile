import os
from django.shortcuts import render
from django.db.models import (
    Count, Max, Min, Avg, Sum, Q, F,
    Case, When, Value,
    IntegerField, CharField,
)
from django.contrib.auth import models as mdlauth
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.login import (
    serializers as srlzlog,
)
from apps.invoice import (
    models as mdlinv,
    serializers as srzinv,
)

from apps.productos import (
    models as mdl,
    serializers as srz,
)
from apps.catalogos import (
    models as mdlcat,
    serializers as srzcat,
)


# Create your views here.

DEFAULT_RESPONSE = {
    "ok": 1,
    "result": 'Request has been processed successfully.',
    "status": 200,
}


class APIGetVendedores(APIView):
    """ Get Sellers List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = mdlcat.Vendedores.objects.filter(
                estado=1,
            )
            wfields = [
                'id', 'nombres',
            ]
            srzoutcome = srzcat.VendedoresSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("nombres") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetTipoSeguimiento(APIView):
    """ Get Tracking Type List"""

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = mdlcat.Combos.objects.filter(
                estado=1, tipo__iexact="CBLF",
            )
            wfields = [
                'id', 'descripcion',
            ]
            srzoutcome = srzcat.CombosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
            return Response(wresponse, status=wresponse.get("status"))
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
            return Response(wresponse, status=wresponse.get("status"))


class APIGetContactados(APIView):
    """ Get Contactados List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = mdlcat.Combos.objects.filter(
                estado=1, tipo__iexact="CBLC",
            )
            wfields = [
                'id', 'descripcion',
            ]
            srzoutcome = srzcat.CombosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetFormaPago(APIView):
    """  Get Payment Type List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.FormaPago.objects.filter(
                    estado=1
                ).select_related("estado")
            )
            wfields = [
                "id", "codigo", "descripcion",
            ]
            srzoutcome = srzcat.FormaPagoSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''

            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(
            wresponse, status=wresponse.get("status")
        )


class APIGetTipoInvoice(APIView):
    """ Get Orders Type """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.TipoInvoice.objects.filter(
                    estado=1
                ).select_related("estado")
            )

            wfields = [
                "id", "descripcion",
            ]
            srzoutcome = srzcat.TipoInvoiceSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetTipificacion(APIView):
    """ Get Order State """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.Tipificaciones.objects.filter(
                    estado=1
                ).select_related("estado")
            )

            wfields = [
                "id", "descripcion",
            ]
            srzoutcome = srzcat.TipificacionesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetPerfilPermisos(APIView):
    """ Get Profile List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = request.GET
            wgroupname = None
            if wdata.get("uid"):
                wgroupname = wdata.get("uid")
                objgroup = mdlauth.Group.objects.filter(
                    id__iexact=wgroupname
                )
                if not objgroup.exists():
                    raise ValueError(str('Error, perfil no existe'))

            fields, error = self.obtener_fields(wgroupname)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            permisos, error = self.obtener_permisos(wgroupname)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            wresponse["result"] = {
                "fields": fields,
                "permisos": permisos,
            }
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))

    def obtener_fields(self, groupname):
        result, error = None, None
        try:
            if not groupname is None:
                objgroup = mdlauth.Group.objects \
                    .filter(id__iexact=groupname) \
                    .select_related("groupextension") \
                    .first()
                wfields = ["id", "name", ]
                srlzoutcome = srlzlog.GroupSerializer(objgroup, fields=wfields)
                result = srlzoutcome.data
            else:
                result = []
        except Exception as ex:
            error = str(ex)
        return [result, error]

    def obtener_permisos(self, groupname):
        result, error = [], None
        try:
            if groupname is None:
                wgroup_id, wgroup_grants = None, []
            else:
                objgroup = mdlauth.Group.objects.filter(
                    id__iexact=groupname).first()
                wgroup_grants = [x.id
                                 for x in objgroup.permissions.all()
                                 .prefetch_related('content_type')]
                wgroup_id = objgroup.id

            woutcome = mdlauth.Permission.objects.all() \
                .prefetch_related("content_type") \
                .annotate(group_id=Case(
                    When(id__in=wgroup_grants,
                         then=Value(wgroup_id)),
                    default=None,
                    output_field=IntegerField()
                )).order_by("content_type_id")

            wfields = ["id", "name", "group_id", ]
            srlzoutcome = srlzlog.GroupSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = item.get("name"), item.get("id")

        except Exception as ex:
            error = str(ex)
        return [result, error]


class APIGetUsuarioPermisos(APIView):
    """ Get User Grants """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            wdata = request.GET
            wusername = None
            if wdata.get("uid"):
                wusername = wdata.get("uid")
                objuser = mdlauth.User.objects.filter(
                    username__iexact=wusername
                )
                if not objuser.exists():
                    raise ValueError(str('Error, usuario no existe'))

            fields, error = self.obtener_fields(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))
            permisos, error = self.obtener_permisos(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))
            perfiles, error = self.obtener_perfiles(wusername)
            if error:
                wresponse["status"] = 422
                raise ValueError(str(error))

            # permisos_clientes, error = self.obtener_perm_clientes(wusername)
            # if error:
            #     wresponse["status"] = 422
            #     raise ValueError(str(error))

            wresponse["result"] = {
                "fields": fields,
                "permisos": permisos,
                "perfiles": perfiles,
                # "clientes": permisos_clientes,
            }
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))

    @classmethod
    def obtener_fields(cls, username):
        """ Get User Values """
        result, error = None, None
        try:
            if username is not None:
                objuser = (
                    mdlauth.User.objects
                    .filter(username__iexact=username)
                    .select_related("userextension")
                    .first()
                )

                wfields = [
                    "id", "first_name", "last_name", "email",
                    "userextension", "is_active", "username",
                    "fullname",
                ]
                srlzoutcome = srlzlog.UserSerializer(
                    objuser, fields=wfields)
                result = srlzoutcome.data
            else:
                result = []
        except Exception as ex:
            error = str(ex)
        return [result, error]

    @classmethod
    def obtener_perfiles(cls, username):
        """ Get Profiles by user """
        result, error = None, None
        try:
            if username is None:
                wuser_id, wuser_profiles = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_profiles = [x.id for x in wuser.groups.all()]

            woutcome = (
                mdlauth.Group.objects.all()
                .annotate(user_id=Case(
                    When(id__in=wuser_profiles, then=Value(wuser_id)),
                    default=None,
                    output_field=IntegerField(),
                ))
            )
            wfields = ["id", "name", "user_id", ]
            srlzoutcome = srlzlog.GroupSerializer(
                woutcome, fields=wfields, many=True)
            result = srlzoutcome.data
            for item in result:
                item["label"] = item.get("name")
                item["key"] = item.get("id")

        except Exception as ex:
            error = str(ex)
        return [result, error]

    @classmethod
    def obtener_permisos(cls, username):
        """ Get Grants by user"""
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_grants = None, []
            else:
                wuser = mdlauth.User.objects.get(
                    username__iexact=username
                )
                wuser_id = wuser.id
                wuser_grants = [
                    x.codename
                    for x in mdlauth.Permission.objects.filter(
                        user__username__iexact=username
                    )
                ]

            woutcome = (
                mdlauth.Permission.objects
                .prefetch_related('content_type')
                .annotate(user_id=Case(
                    When(codename__in=wuser_grants,
                         then=Value(wuser_id)),
                    default=None,
                    output_field=IntegerField(),
                )).order_by("content_type")
            )

            wfields = [
                "id", "name", "codename", "content_type", "user_id",
            ]
            srlzoutcome = srlzlog.PermissionSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                item["label"], item["key"] = (
                    item.get("name"),
                    item.get("id")
                )
        except Exception as ex:
            error = str(ex)
        return [result, error]

    @classmethod
    def obtener_perm_clientes(cls, username):
        """ Get customer list available per user """
        result, error = [], None
        try:
            if username is None:
                wuser_id, wuser_clientes = None, []
            else:
                wuser = mdlauth.User.objects.get(username__iexact=username)
                wuser_id = wuser.id
                wuser_clientes = [
                    x.cliente_id
                    for x in wuser.catalogos_usercliente_user_set.all()
                ]

            woutcome = (
                mdlcat.Clientes.objects.all()
                .select_related("estado")
                .annotate(user_id=Case(
                    When(id__in=wuser_clientes, then=Value(wuser_id)),
                    default=Value(None),
                    output_field=IntegerField()
                )).order_by('-fhregistro')
            )
            wfields = [
                "id", "razon_social", "user_id", "telefono",
            ]
            srlzoutcome = srzcat.ClientesSerializer(
                woutcome, fields=wfields, many=True,
            )
            result = srlzoutcome.data
            for item in result:
                wphone = (item.get("telefono") or '').strip()
                wname = (item.get("razon_social") or '').strip()
                item["label"] = '{} - {}'.format(
                    wname, wphone)
                item["key"] = item.get("id")

        except Exception as ex:
            error = str(ex)
        return [result, error]


class APIGetTipoDocumentoPago(APIView):
    """ Get Document Payment Type List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = mdlcat.TipoDocumentoPago.objects.filter(
                estado=mdlcat.Estados.objects.get(
                    descripcion__exact='ACTIVO'
                )
            )
            wfields = [
                "id", "codigo", "descripcion", "estado",
            ]
            srzoutcome = srzcat.TipoDocumentoPagoSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome

        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetEstadosInvoice(APIView):
    """ Get Invoice status """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlinv.InvoiceEstados.objects
                .filter(estado=1)
                .select_related("estado")
            )
            wfields = [
                "id", "descripcion", "estado",
            ]
            srzoutcome = srzinv.InvoiceEstadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetPaises(APIView):
    """ Get Countries List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.Paises.objects.filter(estado=1)
                .select_related("estado")
            )

            wfields = [
                "id", "codigo", "descripcion", "estado"
            ]
            srzoutcome = srzcat.PaisesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome

        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(
            wresponse, status=wresponse.get("status")
        )


class APIGetClientes(APIView):
    """ Get Customers List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            qfilters = Q(estado=1,)
            wid = request.GET.get("id") or ''
            wfilter = request.GET.get("filter") or ''

            if wfilter != '':
                qfilters.add(
                    Q(razon_social__icontains=wfilter) |
                    Q(ndoc__icontains=wfilter),
                    qfilters.connector,
                )
            if wid != '':
                qfilters.add(
                    Q(id__iexact=wid), qfilters.connector,
                )

            outcome = (
                mdlcat.Clientes.objects.filter(qfilters)
                .select_related("estado")
                .select_related("pais__estado")
                .select_related("tdoc__estado")
            )

            wfields = [
                "id", "razon_social",
                "direccion",
                "tdoc", "ndoc",
                "ciudad", "pais",
                "estado", "telefono",
            ]
            srzoutcome = srzcat.ClientesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                pattern = '{} - {}'.format(
                    item.get("razon_social") or '',
                    item.get("ndoc") or '',
                ).strip()
                item["key"] = item.get("id") or ''
                item["value"] = pattern
                item["label"] = pattern
            wresponse["result"] = srzoutcome

        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetArticulos(APIView):
    """ Get Articles List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            qfilters = Q(
                id__isnull=False,
                estado__id=mdlcat.Estados.Flags.ACTIVE,
            )
            wcategoria = request.GET.get("categoria") or ''
            if wcategoria != '':
                qfilters.add(
                    Q(categoria__id=wcategoria),
                    qfilters.connector,
                )

            outcome = (
                mdl.Articulos.objects.filter(qfilters)
                .select_related("categoria__estado")
                .select_related("estado")
            )

            wfields = ["id", "categoria", "descripcion", "estado"]
            srzoutcome = srz.ArticulosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome

        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetTipoDocumento(APIView):
    """ Get Identity Document Types List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.TipoDocumento.objects.all()
                .select_related("estado")
            )

            wfields = [
                "id", "codigo", "doc_clie",
                "documento", "estado",
            ]
            srzoutcome = srzcat.TipoDocumentoSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("doc_clie") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetCategorias(APIView):
    """ Get Categories List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdl.Categoria.objects.all()
                .select_related("estado")
            )

            wfields = ["id", "descripcion", "estado"]
            srzoutcome = srz.CategoriaSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome

        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetEstados(APIView):
    """ Get General Status List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = mdlcat.Estados.objects.filter(estado=1)
            wfields = [
                'id', 'descripcion', 'estado',
            ]
            srzoutcome = srzcat.EstadosSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(
            wresponse, status=wresponse.get("status")
        )


class APIGetSizes(APIView):
    """ Get Sizes List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.Sizes.objects.filter(
                    estado__id=mdlcat.Estados.Flags.ACTIVE,
                ).select_related("estado")
            )

            wfields = ["id", "descripcion", "estado"]
            srzoutcome = srzcat.SizesSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome

        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))


class APIGetColors(APIView):
    """ Get Colors List """

    def get(self, request):
        wresponse = DEFAULT_RESPONSE.copy()
        try:
            outcome = (
                mdlcat.Color.objects.filter(
                    estado__id=mdlcat.Estados.Flags.ACTIVE,
                ).select_related("estado")
            )

            wfields = ["id", "descripcion", "estado"]
            srzoutcome = srzcat.ColorSerializer(
                outcome, many=True, fields=wfields,
            ).data
            for item in srzoutcome:
                item["key"] = item.get("id") or ''
                item["value"] = item.get("descripcion") or ''
            wresponse["result"] = srzoutcome
        except Exception as ex:
            wresponse['ok'] = 0
            wresponse['result'] = str(ex)
            if wresponse.get('status') == 200:
                wresponse['status'] = 500
        return Response(wresponse, status=wresponse.get("status"))
