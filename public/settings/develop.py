from settings.settings import *


DEBUG = wparam.get('DEBUG') or False
ALLOWED_HOSTS = ['*', ]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': wparam.get('MYSQL_DATABASE'),
        'USER': wparam.get('MYSQL_USER'),
        'PASSWORD': wparam.get('MYSQL_PASSWORD'),
        'HOST': wparam.get('MYSQL_HOST'),
        'PORT': wparam.get('MYSQL_PORT'),
        'CHARSET': 'utf8',
        'COLLATION': 'utf8_general_ci',
        'CREATE_DB': True,
    },
}

STATIC_ROOT = wparam.get("STATIC_ROOT")

INSTALLED_APPS = (
    *INSTALLED_APPS,
    'livereload',
)

MIDDLEWARE = [
    *MIDDLEWARE,
    'livereload.middleware.LiveReloadScript',
]


# Email parameters
EMAIL_HOST = wparam.get('EMAIL_HOST') or ''
EMAIL_PORT = wparam.get('EMAIL_PORT') or 25
if wparam.get("REQUIRE_EMAIL_AUTH"):
    EMAIL_HOST_USER = wparam.get('EMAIL_HOST_USER') or ''
    EMAIL_HOST_PASSWORD = wparam.get('EMAIL_HOST_PASSWORD') or ''
DEFAULT_FROM_EMAIL = wparam.get('DEFAULT_FROM_EMAIL') or ''
SERVER_EMAIL = wparam.get('SERVER_EMAIL') or ''
EMAIL_USE_TLS = wparam.get('EMAIL_USE_TLS') or False
EMAIL_USE_SSL = wparam.get('EMAIL_USE_SSL') or False

# params for email url base
PROJECT_BASE_URL = None
if DEBUG is True:
    PROJECT_BASE_URL = wparam.get('PROJECT_BASE_URL_DEV')
else:
    PROJECT_BASE_URL = wparam.get('PROJECT_BASE_URL')
