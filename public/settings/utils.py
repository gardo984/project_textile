#!/usr/bin/env python
# *-* encoding : UTF-8 *-*

import json
import os
from datetime import datetime
from decouple import config


def replace_params_env(source):
    """ override values if the .env file exists """
    for item in list(source.keys()):
        if item == 'DEBUG':
            field_value = config(item, default=None, cast=bool)
        else:
            field_value = config(item, default=None)
        if field_value is not None:
            source.update({
                str(item): field_value,
            })
    return source


def get_config_parameters():
    """  load the parameters of the json file """
    wpath = os.path.dirname(__file__)
    wconfig = os.path.join(wpath, 'settings.json')
    with open(wconfig, 'r') as f:
        wdata = json.loads(f.read())
        wmirror = dict(wdata)
        for wkey, wvalue in wdata.items():
            if wvalue == '':
                del wmirror[wkey]
        return replace_params_env(wmirror)
    return {}


def default_year():
    return str(datetime.now().strftime('%Y'))
