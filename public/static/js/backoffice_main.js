const listas = {
	list_estados : [],
	list_tdocumento : [],
	list_fpago:[],
	list_clientes : [],
	list_categorias : [],
	list_articulos : [],
	list_tinvoice:[],
	list_seguimiento:[],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;

const searcher = {
	customer:null,
	date_from:null,
	date_to:null,
	order:null,
	date_filter:false,
	sub_tipo_invoice:null,
} ;

const wrequests = { 
	delimiters : ['${','}'],
	name :"wrequests",
	template : '#container-requests',
	data(){
		return {
			search: searcher,
			list : listas,
			details : [],
			item_order : [],
			errors : [],
		}
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods: {
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleExportAll(){
			if(this.lst_items.length==0) return false;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_xls_report',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				if (response.ok){
					swal({
						title:'Aviso',
						text:response.result.message,
						closeOnClickOutside:false,
						closeOnEsc:false,
						icon : 'success',
					}).then(rsp=>{
						window.open(response.result.urlpath,'_blank')
					})
				};
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando exportado de backoffice.';
				swal({
					title:wmessage,
					text: defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleCleanFields(){
			let wdate_to = new Date();
			let wdate_from = wdate_to.addDays(wdate_to,-7) ;
			wdate_from = moment(wdate_from).format("Y-MM-D") 
			wdate_to = moment(wdate_to).format("Y-MM-D") 
			let witems = ['customer','order','date_from','date_to','date_filter','sub_tipo_invoice'] ;
			let wvalues = [null,null,wdate_from,wdate_to,false,null] ;
			for(item in witems) {
				this.search[witems[item]] = wvalues[item] ;
			} ;
		},
		getParameters(){
			return {
				order : this.search.order,
				customer : this.search.customer,
				date_from : this.search.date_from,
				date_to : this.search.date_to,
				date_filter : this.search.date_filter,
				sub_tipo_invoice : this.search.sub_tipo_invoice,
				npage : this.list.page.page_current,
			} ;
		},
		handleNew(){
			this.$emit('change-page','/order') ;
		},
		handleSearch(){
			if(this.areThereErrors) {
				swal('Aviso','Hay campos por completar.','error')
				return false ;
			} ;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_list_items',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de ordenes.',
					icon:'warning',
				});
			}) ;
		},
		modifyOrder(item){
			this.$emit('change-page','/order',item) ;
		},
		StyleRowStatus(item){
			let wclass = '' ;
			if(!item.sub_tipo_invoice) return false;
			switch(item.sub_tipo_invoice.descripcion){
				case 'ENTREGADO':
						wclass = 'flags green'
					break;
				case 'HOLD':
						wclass = 'flags yellow'
					break;
				case 'CANCELADO':
						wclass = 'flags red'
					break;
				case 'CONFIRMADO':
						wclass = 'flags blue'
					break;
				case 'RECHAZADO':
						wclass = 'flags orange'
					break;
			}
			return wclass ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
	},
	computed: {
		areThereErrors(){
			return this.errors.length>0?true:false;
		},
		totales(){
			return {
				filas : this.lst_items.length,
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	watch : {
		'search.date_from': function(wnew, wold){
			this.validatingValue('date_from',wnew) ;
		},
		'search.date_to': function(wnew, wold){
			this.validatingValue('date_to',wnew) ;
		},
	},
	mounted(){
		this.handleCleanFields() ;
		this.handleSearch();
	},
}

const objorder = {
	id:null,
	cliente:null,
	cliente_telefono:null,
	cliente_telefono2:null,
	cliente_ciudad:null,
	cliente_direccion:null,
	observaciones:null,
	tipo_invoice:null,
	forma_pago:null,
	nro_tarjeta:null,
	fecha_venta:null,
	fecha_entrega:null,
	hora_entrega:null,
	// fechahora_entrega:null,
	labora:null,
	contesta_celular:null,
	observaciones2:null,
	sub_tipo_invoice:null,
	delivery_address:null,
	tipo_seguimiento:null,
	vendedor:null,
	tarjeta_vigencia:null,
	tarjeta_cvv:null,
	fecha_etd_courier:null,
	fecha_recepcion:null,
	motivo_reprogramacion:null,
	fecha_entrega_reprogramada:null,
	hora_entrega_reprogramada:null,
	hora_entrega_hasta:null,
}

const wreserva ={ 
	delimiters : ['${','}'],
	name :"wreserva",
	template : '#container-form',
	data(){
		return {
			list : listas,
			details : [],
			item_order : [],
			errors : [],
			fields : objorder,
			range_time: {
				start: '08:30',
				step: '00:15',
				end: '18:30'
			},
			// child : detail_item,
		}
	},
	methods : {
		StyleRowStatus(wposition,witem){
			if(witem.reserva_id) return 'box-busy' ;
			return (wposition % 2==0)?'box-even':'box-odd';
		},
		fixIndexDetails(){
			let x =0 ;
			for(wposition in this.lst_items){
				x+=1;
				this.lst_items[wposition].item = x ;
			} ;
		},
		searchCliente(query){
			 if (query !== '') {
		          this.remote_cliente.loading = true;
		          setTimeout(() => {
		            this.remote_cliente.loading = false;
		            this.remote_cliente.list = this.list.list_clientes.filter(item => {
		              return item.value.toLowerCase()
		                	.indexOf(query.toLowerCase()) > -1;
		            });
		          }, 200);
	        } else {
	         	 this.remote_cliente.list = [];
	        }
		},
		getParameters(){
			return  {
				 id: this.item_order.id ?this.item_order.id:null,
				 order: this.item_order.id ?this.item_order.order:null,
				 delivery_address: this.fields.delivery_address,
				 sub_tipo_invoice: this.fields.sub_tipo_invoice,
				 tipo_seguimiento: this.fields.tipo_seguimiento,
				 hora_entrega_reprogramada : this.fields.hora_entrega_reprogramada?this.fields.hora_entrega_reprogramada:null,
				 fecha_entrega_reprogramada : this.fields.fecha_entrega_reprogramada?moment(this.fields.fecha_entrega_reprogramada).format('Y-MM-D'):null,
				 motivo_reprogramacion: this.fields.motivo_reprogramacion,
				 wtype : 'update_order',
			} ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					// swal('Aviso',response.result.message,"success");
					swal({
						title:'Aviso',
						text:response.result.message,
						icon:'success',
						closeOnClickOutside:false,
						closeOnEsc:false,
					}).then(rsp=>{
						if (!this.item_order.id){
							this.item_order = {
								id:response.result.id,
							};
						} ;
						this.$emit('close-dialog') ;
					});
				}
			}).catch( err=> {
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando guardado de datos en backoffice.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog(){
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		handleCancelOperation(){
			let wmessage  = `Se cancelaran los cambios ingresados, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
				icon:'warning',
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.$emit('close-dialog') ;
						break;
				} ;
			}) ;
		},
		setDefaultValues(){
			// this.fecha_emision = new Date();
			this.fecha_venta = new Date();
		},
		setValues(){
			if(this.item_order){
				this.fields.id = this.item_order.id;
				this.fields.order = this.item_order.order;
				this.fields.cliente = this.item_order.cliente.razon_social ;
	            this.fields.cliente_telefono = this.item_order.cliente_phone;
	            this.fields.cliente_telefono2 = this.item_order.cliente_phone2;
				this.fields.cliente_direccion = this.item_order.cliente_address;
				this.fields.cliente_ciudad = this.item_order.cliente_country;
				this.fields.observaciones = this.item_order.observaciones;
				this.fields.fecha_venta = moment(this.item_order.fecha_venta,"Y-MM-D").format("D/MM/Y"); 
				this.fields.fecha_entrega = this.item_order.fecha_entrega ;
				let whora_entrega =`${this.item_order.fecha_entrega} ${this.item_order.hora_entrega}`;
				let whora_entrega_hasta =`${this.item_order.fecha_entrega} ${this.item_order.hora_entrega_hasta}`;
				this.fields.hora_entrega = moment(whora_entrega,"Y-MM-D H:mm:ss").format("H:mm");
				this.fields.hora_entrega_hasta = moment(whora_entrega_hasta,"Y-MM-D H:mm:ss").format("H:mm");

				// this.fields.fechahora_entrega = moment(whora_entrega,"Y-MM-D H:mm:ss").format("D/MM/Y HH:mm:ss");
				this.fields.tipo_invoice = this.item_order.tipo_invoice?this.item_order.tipo_invoice.descripcion:null;
				this.fields.forma_pago = this.item_order.forma_pago?this.item_order.forma_pago.descripcion:null;
				this.fields.nro_tarjeta = this.item_order.nro_tarjeta ;
				this.fields.observaciones2 = this.item_order.observaciones2;
				this.fields.labora	= this.item_order.labora;
				this.fields.contesta_celular = this.item_order.contesta_celular;
				this.fields.delivery_address = this.item_order.delivery_address ;
				this.fields.sub_tipo_invoice = this.item_order.sub_tipo_invoice?this.item_order.sub_tipo_invoice.id:null;
				this.fields.tipo_seguimiento = this.item_order.tipo_seguimiento?this.item_order.tipo_seguimiento.id:null;
				this.fields.vendedor = this.item_order.vendedor?this.item_order.vendedor.nombres:null;

				if(this.item_order.fecha_etd_courier!=null){
					this.fields.fecha_etd_courier = moment(this.item_order.fecha_etd_courier,"Y-MM-D").format("D/MM/Y"); 	
				}else{
					this.fields.fecha_etd_courier=null ;
				}
				if(this.item_order.fecha_recepcion!=null){
					this.fields.fecha_recepcion = moment(this.item_order.fecha_recepcion,"Y-MM-D").format("D/MM/Y"); 
				}else{
					this.fields.fecha_recepcion=null;
				}
				this.fields.tarjeta_vigencia = this.item_order.tarjeta_vigencia;
				this.fields.tarjeta_cvv = this.item_order.tarjeta_cvv;

				this.fields.fecha_entrega_reprogramada = this.item_order.fecha_entrega_reprogramada?this.item_order.fecha_entrega_reprogramada:null ;
				if(this.fields.fecha_entrega_reprogramada!=null){
					let whora_entrega_reprogramada =`${this.item_order.fecha_entrega_reprogramada} ${this.item_order.hora_entrega_reprogramada}`;
					this.fields.hora_entrega_reprogramada = moment(whora_entrega_reprogramada,"Y-MM-D H:mm:ss").format("H:mm");
				}else{
					this.fields.hora_entrega_reprogramada=null ;
				}
				this.fields.motivo_reprogramacion = this.item_order.motivo_reprogramacion;
			} ;
		},
		getOrderItem(wid){
			let wparameters = {
				id:wid, 
				wtype : 'get_item_values',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers : axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.item_order = response.result;
				this.details = this.item_order.invoicedetails_set ;
				this.setValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo backoffice.',
					icon:'warning',
				});
			}) ;	
		},
		validatingStatus(){
			let wparams = this.$route.params ;
			if(Object.keys(wparams).length>0){
				this.getOrderItem(wparams.uid);
			}else{
				this.setDefaultValues();
				this.$refs.cliente.focus() ;
			} ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	watch : {
		
	},
	computed : {
		fechahora_entrega(){
			return moment(this.fields.fecha_entrega,"Y-MM-D").format("D/MM/Y") + 
					' ' + this.fields.hora_entrega + 
					' - '+this.fields.hora_entrega_hasta;
		},
		status_reprograma(){
			if(this.fields.tipo_seguimiento==8){
				return false ;
			}else{
				return true ;
			}
		},
		formStatus(){
			return true ;
		},
		areThereErrors(){
			let wfields = [
				
			]
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
		summary(){
			let _total = 0;
			let _subtotal = 0;
			for(item in this.lst_items){
				_subtotal += parseFloat(this.lst_items[item].subtotal||0) ;
				_total += parseFloat(this.lst_items[item].total||0) ;
			};
			return {
				subtotal: _subtotal.toFixed(2),
				total: _total.toFixed(2),
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return row.visible==true ;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	created(){
				
	},
	mounted(){
		this.validatingStatus();
	},
}

const routes =[
	{path:'/',component: wrequests,},
	{ path:'/order',component: wreserva,},
	{ 
		path:'/order/:uid',
		component: wreserva,
		props : true,
	},
]  ;
const router = new VueRouter({
	routes: routes
})
const vcontainer = new Vue({
	delimiters : ['${','}'],
	el : "#vTabs",
	data : {
		list : listas,
	},
	router,
	methods : {
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADOS_VENTA,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estado venta.',
					icon:'warning',
				});
			}) ;
		},
		/* added at 2019.09.02 */
		getTipoSeguimiento(){
			axios({
				method : 'get',
				url: URI_TIPO_SEG,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_seguimiento = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de seguimiento.',
					icon:'warning',
				});
			}) ;
		},
		onChangePage(wpage,wparams){
			let wurl = wpage ;
			if (wparams){
				wurl = `${wurl}/${wparams.id}`
			};
			this.$router.push({
				path:wurl,
				params: wparams,
			});
		},
		onCloseDialog(){
			this.$router.push({path:'/'});	
		},
	},
	mounted(){
		this.getEstados();
		this.getTipoSeguimiento();
	},
}) ;
