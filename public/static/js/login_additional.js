

const vApp = new Vue({
	el:'#vContainer',
	data : {
		firstname :null,
		lastname:null,
		email :null,
		passwd : null,
		errors : [],
		loading: false,
	},
	methods : {
		getParameters(){
			return {
				first_name : this.firstname,
				last_name: this.lastname,
				email: this.email,
				password: this.passwd,
			};
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		},
		setValues(source){
			if (source){
				this.firstname = source.first_name;
				this.lastname = source.last_name;
				this.email = source.email;
			} ;
		},
		cleanFields(){
			let wfields = [
				'firstname','lastname','email','passwd',
			] ;
			let wvalues = [null,null,null,null,] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item] ;
			}
		},
		processUpdate(){
			let wparameters = this.getParameters();
			Object.assign(wparameters,{
				wtype : 'save_user_info',
			}) ;
			this.loading=true;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				this.loading=false;
				let response  = rsp.data ;
				if(response.ok){
					swal({
						title:'Aviso',
						text: response.result,
						icon:"success",
						closeOnEsc :false,
						closeOnClickOutside :false,
					}) ;
					this.handleGetUserInformation();
				} ;
			}).catch( err=> {
				this.loading=false;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg  = 'Error, procesando guardado de datos informacion de usuario.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleUpdateInformation(){
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá con el guardado de datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.processUpdate() ;
						break;
				} ;
			}) ;
		},
		handleGetUserInformation(){
			let wparameters = {
				uid: udefault,
				wtype:'get_user_info',
			}
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					this.setValues(response.result)
				} ;
			}).catch( err=> {
				this.loading=false;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg  = 'Error, obteniendo informacion de usuario.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
	},
	computed: {
		handleDisabled(){
			return this.errors.length>0?true:false ;
		},
		areThereErrors(){
			let wfields = [
				'firstname','lastname','email','passwd',
			]
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
	},
	watch: {
		'firstname' : function(wnew,wold){
			this.validatingValue('firstname',wnew) ;
		},
		'lastname' : function(wnew,wold){
			this.validatingValue('lastname',wnew) ;
		},
		'email' : function(wnew,wold){
			this.validatingValue('email',wnew) ;
		},
		'passwd' : function(wnew,wold){
			this.validatingValue('passwd',wnew) ;
		},
	},
	mounted(){
		this.handleGetUserInformation();
	},
}) ;