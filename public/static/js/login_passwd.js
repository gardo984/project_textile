

const vApp = new Vue({
	el:'#vContainer',
	data : {
		passwd : null,
		new_passwd :null,
		new_passwd2 : null,
		errors : [],
		loading: false,
	},
	methods : {
		getParameters(){
			return {
				password : this.passwd,
				new_password: this.new_passwd,
				new_password2: this.new_passwd2,
			};
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		},
		cleanFields(){
			let wfields = [
				'passwd','new_passwd','new_passwd2',
			] ;
			let wvalues = [null,null,null,] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item] ;
			}
		},
		processChangePasswd(){
			let wparameters = this.getParameters();
			this.loading=true;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				this.loading=false;
				let response  = rsp.data ;
				if(response.ok){
					swal({
						title:'Aviso',
						text: response.result,
						icon:"success",
						closeOnEsc :false,
						closeOnClickOutside :false,
					}).then(rsp=>{
						window.location = URI_HOME;
					});
				} ;
			}).catch( err=> {
				this.loading=false;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg  = 'Error, procesando guardado de datos en reservas.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleChangePasswd(){
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá con el cambio de Clave, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.processChangePasswd() ;
						break;
				} ;
			}) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
	},
	computed: {
		handleDisabled(){
			return this.errors.length>0?true:false ;
		},
		areThereErrors(){
			let wfields = [
				'passwd','new_passwd','new_passwd2',
			]
			for(item in wfields){
				this.validatingValue(wfields[item],this[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
	},
	watch: {
		'passwd' : function(wnew,wold){
			this.validatingValue('passwd',wnew) ;
		},
		'new_passwd' : function(wnew,wold){
			this.validatingValue('new_passwd',wnew) ;
		},
		'new_passwd2' : function(wnew,wold){
			this.validatingValue('new_passwd2',wnew) ;
		},
	},
	mounted(){

	},
}) ;