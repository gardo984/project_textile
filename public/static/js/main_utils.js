function displayExpirationAlert(){
	if($('.session-side').length>0){
		$('.session-side').toggleClass('appear') ;
	} ;
} ;

// get the session time left 
function getIntervalLeft(){
	if($('#h_deadline').length==0) return false;
	let wmininum= $('#h_minimun').val();
	let to = $('#h_deadline').val();
	let d = new Date();
	let wfrom = new Date(d.toString()) ;
	let wto = new Date(to) ;
	let x = (wto - wfrom) ;
	if ((wmininum*1000)>=x){
		if($('.session-side.appear').length==0){
			displayExpirationAlert();
		} ;
	} ;
	let diff_hours = parseInt((x/1000)/60/60)
	let diff_minutes = parseInt((x/1000)/60) - (diff_hours*60) ;
	let diff_seconds = (x/1000) - ((diff_minutes + (diff_hours*60)) * 60);
	let hour = (diff_hours<10?'0'+diff_hours:diff_hours) ;
	let minute = (diff_minutes<10?'0'+diff_minutes:diff_minutes) ;
	let seconds = (diff_seconds<10?'0'+diff_seconds:diff_seconds) ;
	let outcome =  {
		minutes : parseInt(minute),
		seconds : parseInt(seconds),
		hours : parseInt(hour),
		text : hour+':'+minute+':'+seconds,
	} ;
	if(outcome.minutes==0){
		$('.info-session').html(`<b>Tiempo Sesión</b> : -`) ;
		return false;
	} ;
	$('.info-session').html(`<b>Tiempo Sesión</b> : ${outcome.text}`) ;

	// set value into expiration alert
	if($('.session-body p label').length>0){
		let label = $('.session-body p label') ;
		label.text(outcome.text) ;
	} ;
} ;

// get highest zindex property of the dom
function getHigherZindexElement(){
	return Array.from(document.querySelectorAll("body *"))
		.map(x=> parseFloat(window.getComputedStyle(x).zIndex))
		.filter(y=> !isNaN(y))
		.sort((a,b)=> a-b)
		.pop() ;
} ;

// modify default behavior of XMLHttpRequest 
// in order to show a spinning loader is every ajax call
var oldxhr= window.XMLHttpRequest;
function newXHR(){
	var request = new oldxhr();
	request.onreadystatechange = function(){
		let lastIndex = getHigherZindexElement();
		$('.middle-side .middle-rightside-loader').css({ 
			"z-index":(lastIndex+1), 
		}) ;
		$('.middle-side .middle-rightside-loader').toggleClass('appear') ;
	} ;
	request.onload=function(){
		// if(request.status==200){

		// } else if (request.status!==200){

		// } ;
		$('.middle-side .middle-rightside-loader').toggleClass('appear') ;
	};
	return request ;
} ;
window.XMLHttpRequest = newXHR ;

// filter value into object list
function search_filter(pattern, search){
	if (pattern == null || pattern =='') return true;
	let wpattern = new RegExp(pattern,'i') ;
	if(search ===undefined || wpattern.test(search)==false ){
		throw 'value no found';
	}
	return true ;
};

// clean each parameter of an object 
function cleanObject(obj){
	Object.keys(obj).map(row => {
		if(obj[row] instanceof Object){
			cleanObject(obj[row]) ;
			return ;
		} ;
		if(obj[row] instanceof Array){
			obj[row] =[] ;
		}else{
			obj[row] = null ;
		} ;
	}) ;
}

// Used to detect whether the users browser is an mobile browser
function isMobile() {
    ///<summary>Detecting whether the browser is a mobile browser or desktop browser</summary>
    ///<returns>A boolean value indicating whether the browser is a mobile browser or not</returns>

    if (sessionStorage.desktop) // desktop storage 
        return false;
    else if (localStorage.mobile) // mobile storage
        return true;

    // alternative
    var mobile = ['iphone','ipad','android','blackberry','nokia','opera mini','windows mobile','windows phone','iemobile']; 
    for (var i in mobile) if (navigator.userAgent.toLowerCase().indexOf(mobile[i].toLowerCase()) > 0) return true;

    // nothing found.. assume desktop
    return false;
}; 

// date functions

Date.prototype.addDays = function(wdate,wdays){
	let current_date = new Date(wdate) ;
	current_date.setDate(current_date.getDate()+wdays) ;
	return new Date(current_date) ;
} ;

Date.prototype.get_date = function(wtype){
	let x = new Date();
	let day = (x.getDate()<10?'0'+x.getDate():x.getDate()) ;
	let month = (x.getMonth()<10?'0'+x.getMonth():x.getMonth()) ;
	let xdate = day +'/'+ month +'/'+x.getFullYear();
	if(wtype=='native'){
		xdate = month +'/'+ day +'/'+x.getFullYear();
	} ;
	return xdate ;
} ;
Date.prototype.get_time = function(wdate){
	let x = wdate? wdate:(new Date()) ;
	let hour = (x.getHours()<10?'0'+x.getHours():x.getHours()) ;
	let minute = (x.getMinutes()<10?'0'+x.getMinutes():x.getMinutes()) ;
	let seconds = (x.getSeconds()<10?'0'+x.getSeconds():x.getSeconds()) ;
	let xtime = hour+':'+ minute +':'+seconds ;
	return xtime ;
} ;
Date.prototype.nowString = function(){
	let x = new Date();
	return x.get_date() +' ' + x.get_time() ;
} ;