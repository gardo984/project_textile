

const listas = {
	list_estados : [],
	list_tdocumento : [],
	list_tdocpago : [],
	list_clientes : [],
	list_categorias : [],
	list_articulos : [],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;

const detail_item = {
	item : null,
	producto : {
		id :null,
		descripcion : null,
		categoria : {
			id:null,
			descripcion:null,
		}
	},
	cantidad : null,
	subtotal : null,
}

const fields = {
	id:null,
	documento_compra:null,
	documento_fecha_emision:null,
	observaciones:null,
	details : [],
}

const wrequests = { 
	delimiters : ['${','}'],
	name :"wrequests",
	template : '#container-requests',
	data(){
		return {
			descripcion : null,
			fecha_desde : null,
			fecha_hasta : null,
			list : listas,
			details : [],
			item : [],
			errors : [],
		}
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods: {
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleExportAll(){
			if(this.lst_items.length==0) return false;
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_xls_report',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				if (response.ok){
					swal({
						title:'Aviso',
						text:response.result.message,
						closeOnClickOutside:false,
						closeOnEsc:false,
						icon : 'success',
					}).then(rsp=>{
						window.open(response.result.urlpath,'_blank')
					})
				};
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando exportado de orden compra.';
				swal({
					title:wmessage,
					text: defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleCleanFields(){
			let wdate_to = new Date();
			let wdate_from = wdate_to.addDays(wdate_to,-7) ;
			wdate_from = moment(wdate_from).format("Y-MM-D") 
			wdate_to = moment(wdate_to).format("Y-MM-D") 
			let witems = ['descripcion','fecha_desde','fecha_hasta',] ;
			let wvalues = [null,wdate_from,wdate_to,] ;
			for(item in witems) {
				this[witems[item]] = wvalues[item] ;
			} ;
		},
		getParameters(){
			return {
				descripcion:this.descripcion,
				fecha_desde : this.fecha_desde,
				fecha_hasta : this.fecha_hasta,
				npage : this.list.page.page_current,
			} ;
		},
		handleNew(){
			this.$emit('change-page','/order') ;
		},
		handleSearch(){
			let wparameters = this.getParameters();
			Object.assign(wparameters, {
				wtype : 'get_ordenes_compra',
			})
			axios({
				method : 'post',
				url:URI_HOME,
				data : wparameters,
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response.data) ;
				let response = err.response ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, obteniendo listado de ordenes compra.';
				swal({
					title:wmessage,
					text: response.data.result | defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		modifyItem(item){
			this.$emit('change-page','/order',item) ;
		},
		removeItem(item){
			this.item = item;
			let wmessage  = `Se procederá con la anulación ` +
				 `de orden de compra nro. ${item.id}, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		removeProcess(){
			let wparameters = {
				wtype : 'remove_orden_compra',
				item : this.item,
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let response = err.response ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					let defaultmsg = 'Error, anulando orden de compra.';
					swal({
						title:wmessage,
						text: response.data.result | defaultmsg,
						icon:'warning',
					});
				}
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
	},
	computed: {
		areThereErrors(){
			return this.errors.length>0?true:false;
		},
		totales(){
			let wrows = this.lst_items.length ;
			let wtotal = this.list.page.page_total;
			return {
				nitems: wrows,
				ntotal: wtotal,
			} ;
		},
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	watch : {

	},
	mounted(){
		this.handleCleanFields() ;
		this.handleSearch();
	},
}

const wreserva ={ 
	delimiters : ['${','}'],
	name :"wreserva",
	template : '#container-form',
	data(){
		return {
			list : listas,
			form : fields,
			child : detail_item,
			item : [],
			errors : [],
		}
	},
	methods : {
		getArticulos(){
			if (!this.child.producto.categoria.id){
				return false;
			} ;
			axios({
				method : 'get',
				url: URI_ARTICULOS,
				params : { 
					categoria: this.child.producto.categoria.id,
				},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_articulos = response.result;
			}).catch( err=> {
				if (err.response) {
					console.log(err.response.data) ;
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de articulos.',
						icon:'warning',
					});
				}
			}) ;
		},
		StyleRowStatus(wposition,witem){
			if(witem.reserva_id) return 'box-busy' ;
			return (wposition % 2==0)?'box-even':'box-odd';
		},
		handleChangeCantidad(){

		},
		handleAddArticle(){
			if (!this.child.producto.categoria.id){
				swal('Error','Seleccione categoria a agregar.','error') ;
				return false;
			} ;
			if (!this.child.producto.id){
				swal('Error','Seleccione articulo a agregar.','error') ;
				return false;
			} ;
			if(this.child.producto.id){
				let witem = this.child.producto.id ;
				let warticle = this.list.list_articulos.filter(row=> {
					return row.id ==  witem ;
				}) ;
				let wcantidad = parseInt(this.child.cantidad||1);
				if (warticle.length>0) {
					this.form.details.push({
						check : false,
						item : (this.lst_items.length+1),
						producto : {
							id : warticle[0].id,
							descripcion : warticle[0].value,
							categoria : {
								id : warticle[0].categoria.id,
								descripcion : warticle[0].categoria.descripcion,
							},
						},
						cantidad : this.child.cantidad,
						visible : true,
					}) ;
					this.handleCleanDetailFields();
				}
			} ;
		},
		handleUpdateArticle(){
			let witem = this.child ;;
			let warticle = this.list.list_articulos.filter(row=> {
				return row.id ==  witem.producto.id ;
			}) ;
			let wcantidad = parseInt(this.child.cantidad||1);
			this.form.details.forEach(function(wobj,windex){
				if(wobj.item == witem.item){
					wobj.producto.id = warticle[0].id ;
					wobj.producto.descripcion = warticle[0].descripcion ;
					wobj.producto.categoria.id = warticle[0].categoria.id ;
					wobj.producto.categoria.descripcion = warticle[0].categoria.descripcion ;
					wobj.cantidad = wcantidad ;
					cleanObject(witem) ;
					return ;
				} ;
			}) ;

		},
		handleCancelArticleChanges(){
			this.handleCleanDetailFields() ;
		},
		handleCleanDetailFields(){
			cleanObject(this.child) ;
		},
		fixIndexDetails(){
			let x =0 ;
			for(wposition in this.lst_items){
				x+=1;
				this.lst_items[wposition].item = x ;
			} ;
		},
		handleModifyArticle(item){
			if(item.item){
				this.child.item = item.item ;
				this.child.producto.id = item.producto.id ;
				this.child.producto.categoria.id = item.producto.categoria.id ;
				this.child.cantidad = item.cantidad ;
			}
		},
		handleRemoveArticle(item){
			if(item){
				let wmessage = `Se procederá con la eliminación de ${item.producto.descripcion}, ¿Desea continuar?` ;
				swal({
					title : 'Confirmación',
					text : wmessage,
					buttons : {
						yes: 'Si',
						no: 'No',
					},
					icon : 'info',
				}).then(rsp=>{
					switch(rsp){
						case 'yes' :
								this.lst_items.forEach((wobj,windex)=> {
									if(wobj.item == item.item){
										wobj.visible = false ;
										return ;
									} ;
									this.fixIndexDetails();
								}) ;
							break;
					}
				}) ;
			} ;
		},
		handleCleanFiedls(){
			cleanObject(this.form);
			cleanObject(this.child);
		},
		getParameters(){
			return  {
				 id: this.form.id,
				 documento_compra:this.form.documento_compra,
				 documento_fecha_emision:this.form.documento_fecha_emision,
				 observaciones:this.form.observaciones,
				 details : this.lst_items,
				 wtype : this.form.id!=null ?'update_orden_compra':'add_orden_compra',
			} ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					if (!this.form.id){
						this.form.id = response.result.id;
					} ;
					
					// this.$emit('close-dialog') ;
				}
			}).catch( err=> {
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let defaultmsg = 'Error, procesando guardado de datos en orden de compra.';
				swal({
					title:wmessage,
					text: err.response.data.result||defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog(){
			// if(this.areThereErrors){
			// 	swal('Aviso','Hay campos por completar.','error') ;
			// 	return false;
			// } ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",no : "No",
				},
				icon : 'info'
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation() ;
						break;
				} ;
			}) ;
		},
		handleCancelOperation(){
			let wmessage  = `Se cancelaran los cambios ingresados, ¿Desea Continuar?` ;
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
				icon:'warning',
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.handleCleanFiedls()
							this.$emit('close-dialog') ;
						break;
				} ;
			}) ;
		},
		setDefaultValues(){
			
		},
		setValues(){
			if(this.item.id){
				this.form.id = this.item.id;
				this.form.documento_compra = this.item.documento_compra;
				this.form.documento_fecha_emision = this.item.documento_fecha_emision;
				this.form.observaciones = this.item.observaciones;
				this.form.details = this.item.ordencompradetails_set;
			} ;
		},
		getItem(wid){
			let wparameters = {
				id:wid, 
				wtype : 'get_orden_compra',
			} ;
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers : axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.item = response.result;
				this.setValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response = err.response;
				let defaultmsg = 'Error, obteniendo orden de compra.' ;
				swal({
					title:wmessage,
					text: response.data.result|defaultmsg,
					icon:'warning',
				});
			}) ;	
		},
		validatingStatus(){
			let wparams = this.$route.params ;
			if(Object.keys(wparams).length>0){
				this.getItem(wparams.uid);
			}else{
				this.setDefaultValues();
				/*this.$refs.cliente.focus() ;*/
			} ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	watch : {
		'child.producto.categoria.id': function(wnew,wold){
			this.getArticulos();
		},
	},
	computed : {
		areThereErrors(){
			let wfields = []
			for(item in wfields){
				this.validatingValue(wfields[item],this.form[wfields[item]])
			}
			return this.errors.length>0?true:false;
		},
		summary(){
			return {
				nitems: this.lst_items.length,
				nunits: 0,
			} ;
		},
		lst_items(){
			return this.form.details.filter(row=>{
				try {
					return row.visible==true ;
				} catch (ex){
					return false;
				}
				return ;
			}) ;
		},
	},
	created(){
				
	},
	mounted(){
		this.validatingStatus();
	},
}

const routes =[
	{path:'/',component: wrequests,},
	{ path:'/order',component: wreserva,},
	{ 
		path:'/order/:uid',
		component: wreserva,
		props : true,
	},
]  ;
const router = new VueRouter({
	routes: routes
})
const vcontainer = new Vue({
	delimiters : ['${','}'],
	el : "#vTabs",
	data : {
		list : listas,
	},
	router,
	methods : {
		getCategorias(){
			axios({
				method : 'get',
				url: URI_CATEGORIAS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_categorias = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de categorias.',
					icon:'warning',
				});
			}) ;
		},
		onChangePage(wpage,wparams){
			let wurl = wpage ;
			if (wparams){
				wurl = `${wurl}/${wparams.id}`
			};
			this.$router.push({
				path:wurl,
				params: wparams,
			});
		},
		onCloseDialog(){
			this.$router.push({path:'/'});	
		},
	},
	mounted(){
		this.getCategorias();
	},
}) ;
