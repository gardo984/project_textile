
const listas = {
	list_estados : [],
	list_concesionarios : [],
	list_paises : [],
	list_perfiles : [],
	list_accesos : [],
	list_clientes : [],
	list_confirmacion : [
		{id:1, value:"SI",},
		{id:2, value:"NO",},
	],
	page : {
		page_size:0,
		page_total:0,
		page_count:7,
		page_current:1,
	},
} ;
const vApp = new Vue({
	delimiters : ['${','}'],
	el : '#vContainer',
	data: {
		descripcion:null,
		estado :null,
		list : listas,
		details : [],
		item : [],
		visible_status:false,
	},
	filters : {
		format : function(date_value, format){
			if (date_value == null) {
				return '-' ;
			} ;
			return moment(date_value).format(format) ;
		},
	},
	methods : {
		handleClean(){
			let wfields = [ 
				'descripcion','estado',
			] ;
			let wvalues = [ null,null, ] ;
			for(item in wfields){
				this[wfields[item]] = wvalues[item];
			};
			this.list.page.page_current = 1;
		},
		getParameters(){
			return {
				descripcion : this.descripcion || null,
				estado : this.estado || null,
				npage : this.list.page.page_current,
			} ;
		},
		getConcesionarios(){
			axios({
				method : 'get',
				url: URI_CONCESIONARIOS,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_concesionarios = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de concesionarios.',
					icon:'warning',
				});
			}) ;
		},
		getClientes(){
			axios({
				method : 'get',
				url: URI_CLIENTES,
				parameters : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_clientes = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de clientes.',
					icon:'warning',
				});
			}) ;
		},
		getEstados(){
			axios({
				method : 'get',
				url: URI_ESTADO,
				params : {},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_estados = response.result;
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo listado de estados.',
					icon:'warning',
				});
			}) ;
		},
		
		removeProcess(){
			let wparameters = { 
				id: this.item.id,
			} ;
			axios({
				method : 'post',
				url: URI_REMOVE,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					this.item = [],
					this.handleSearch();
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response  = err.response ;
				let defaultmsg = 'Error, procesando eliminacion usuario.';
				swal({
					title:wmessage,
					text: response.data?response.data.result:defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleModify(item){
			this.item = item ;
			this.visible_status = true ;
			vModal.dialog_status = true ;
			vModal.setDefaultValues();
		},
		handleRemove(item){
			this.item = item;
			let wmessage  = `Se procederá con el eliminado de usuario : ${this.item.fullname}, ¿Desea Continuar?`
			swal({
				title:'Confirmación',
				text: wmessage,
				buttons : {
					yes:'Si',
					no:'No',
				},
			}).then(rsp => {
				switch(rsp){
					case 'yes':
							this.removeProcess();
						break;
				} ;
			}) ;
		},
		handleChangePage(npage){
			this.list.page.page_current = npage;
			this.handleSearch();
		},
		handleSearch(){
			let wparameters = this.getParameters()
			axios({
				method : 'post',
				url: URI_HOME,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				this.details = response.result.rows ;
				this.list.page.page_total = response.result.page.total;
				this.list.page.page_size = response.result.page.size;
				this.list.page.page_count = response.result.page.count;
			}).catch( err=> {
				console.log(err.response) ;
				if(err.response){
					let wmessage = err.response.status+ ' ' +err.response.statusText ;
					swal({
						title:wmessage,
						text: 'Error, obteniendo listado de usuarios.',
						icon:'warning',
					});
				}
			}) 
		},
		handleAddItem(){
			vModal.dialog_status=true ;
			this.visible_status=true;
			vModal.form.estado=1;
			vModal.form.all_customer=2;
			vModal.form.all_orders=2;
			vModal.getUserGrants();
			// vModal.handleProfilesList();
		},
	},
	computed : {
		lst_items(){
			return this.details.filter(row=>{
				try {
					return true;
				} catch (ex){
					return false;
				}
				return ;
			});
		},
		totales(){
			return {
				filas: this.lst_items.length,
			} ;
		},
	},
	mounted(){
		// this.getClientes();
		this.getEstados();
		this.handleSearch();
	},
}) ;

const fields = {
	id:null,
	username : null,
	password:null,
	first_name : null,
	last_name:null,
	dni:null,
	email:null,
	estado:null,
	perfiles : [],
	accesos : [],
	clientes: [],
	all_customer:null,
	all_orders:null,
}
const vModal = new Vue({
	delimiters : ['${','}'],
	el: '#vModal',
	data : {
		dialog_status : false,
		form : fields,
		list : listas,
		errors:[],
	},
	methods : {
		handleProfilesFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
        handleGrantsFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
        handleClienteFilter(query, item) {
         	return item.label.toLowerCase().indexOf(query.toLowerCase()) > -1;
        },
		setDefaultValues(){
			if(vApp.item.id){
				this.form.id = vApp.item.id ;
				this.form.username = vApp.item.username ;
				this.form.first_name = vApp.item.first_name ;
				this.form.last_name = vApp.item.last_name;
				this.form.estado = vApp.item.is_active?1:2 ;
				this.form.email = vApp.item.email;
				this.form.dni = vApp.item.userextension?vApp.item.userextension.dni:null;
				this.form.all_customer = vApp.item.userextension?vApp.item.userextension.all_customers:2;
				this.form.all_orders = vApp.item.userextension?vApp.item.userextension.all_orders:2;
				this.getUserGrants()
			}
		},
		setGrantValues(){
			let wlist = [
				this.list.list_accesos, this.list.list_perfiles,
				// this.list.list_clientes,
			]
			let wsource = [
				vModal.form.accesos, vModal.form.perfiles,
				// vModal.form.clientes,
			]
			for (item in wlist){
				wlist[item].forEach((obj,index)=>{
					if(obj.user_id!=null){
						wsource[item].push(obj.id) ;
					}
				}) ;	
			}
		},
		getUserGrants(){
			axios({
				method : 'get',
				url: URI_GRANTS,
				params : {uid:this.form.username||null,},
				responseType : 'json',
			}).then(rsp=> {
				let response  = rsp.data ;
				this.list.list_perfiles = response.result.perfiles;
				this.list.list_accesos = response.result.permisos;
				// this.list.list_clientes = response.result.clientes;
				this.setGrantValues();
			}).catch( err=> {
				console.log(err.response.data) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				swal({
					title:wmessage,
					text: 'Error, obteniendo datos de usuario.',
					icon:'warning',
				});
			}) ;
		},
		getParameters(){
			return {
				fields : {
					id : this.form.id,
					clave : this.form.password,
					usuario : this.form.username,
					first_name : this.form.first_name,
					last_name : this.form.last_name,
					is_active : this.form.estado==1?1:0,
					email : this.form.email,
					dni : this.form.dni,
					all_customers : this.form.all_customer,
					all_orders : this.form.all_orders,
				},
				permissions: this.form.accesos,
				groups: this.form.perfiles,
				// clientes: this.form.clientes,
			} ;
		},
		cleanFields(){
			cleanObject(vModal.form) ;
			// let ls = ['perfiles','accesos','clientes'];
			let ls = ['perfiles','accesos',];
			ls.forEach((wobj,windex)=>{
				vModal.form[wobj] = [];
			});
			this.errors = [];
			let ltransfer =  [
				this.$refs.elt_perfiles, this.$refs.elt_permisos,
				// this.$refs.elt_clientes,
			]
			for(x in ltransfer){
				ltransfer[x].clearQuery("left");
				ltransfer[x].clearQuery("right");
			}
			if(vApp.item){
				vApp.item= [];
			} ;
		},
		handleCloseDialog() {
			this.dialog_status = false ;
			vApp.visible_status = false ;
			this.cleanFields();
		},
		handleCancelDialog() {
			this.handleCloseDialog() ;
		},
		saveInformation(){
			let wparameters = this.getParameters();
			let wpath = !this.form.id?URI_APPEND:URI_UPDATE;
			axios({
				method : 'post',
				url: wpath,
				data : wparameters,
				responseType : 'json',
				headers: axiosPostHeaders,
			}).then(rsp=> {
				let response  = rsp.data ;
				if(response.ok){
					swal('Aviso',response.result.message,"success");
					vApp.handleSearch();
					this.handleCloseDialog()
				}
			}).catch( err=> {
				console.log(err.response) ;
				let wmessage = err.response.status+ ' ' +err.response.statusText ;
				let response = err.response ;
				let defaultmsg = 'Error, procesando guardado de datos sucursal.';
				swal({
					title:wmessage,
					text: response.data? response.data.result:defaultmsg,
					icon:'warning',
				});
			}) ;
		},
		handleConfirmDialog() {
			if(this.areThereErrors){
				swal('Aviso','Hay campos por completar.','error') ;
				return false;
			} ;
			swal({
				title : 'Confirmación',
				text:'Se procederá a guardar los datos, ¿Desea continuar?.',
				buttons : {
					yes : "Si",
					no : "No",
				},
			}).then(rsp => {
				switch(rsp) {
					case 'yes':
							this.saveInformation();
						break;
				} ;
			}) ;
		},
		validatingValue(welement,wvalue){
			if (!wvalue || wvalue==null || wvalue=='') {
				let status = this.errors.filter(row=>{
					return row==welement
				}) ;
				if(status.length>0) return ;
				this.errors.push(welement) ;
				return  ;
			}
			let windex = this.errors.indexOf(welement) ;
			if (windex>-1) this.errors.splice(windex,1) ;
		},
		hasError(wfield){
			for(item in this.errors){
				if(this.errors[item]==wfield) return true;
			}
			return false;
		},
		handleBlur(e){
			if(e.target) {
				this.validatingValue(e.target.name,e.target.value) ;
				return ;
			} ;
			if(e.name) {
				this.validatingValue(e.name,e.value) ;
				return ;
			} ;
		}
	},
	computed:{
		areThereErrors(){
			let wfields = [ 
				'first_name','last_name','dni',
				'email','estado',
				'all_customer','all_orders',
			]
			for(item in wfields){
				this.validatingValue(
					wfields[item],
					this.form[wfields[item]]
				)
			}
			return this.errors.length>0?true:false;
		},
	},
	watch:{
		'form.first_name' : function(wnew,wold){
			this.validatingValue('first_name',wnew) ;
		},
		'form.last_name' : function(wnew,wold){
			this.validatingValue('last_name',wnew) ;
		},
		'form.dni' : function(wnew,wold){
			this.validatingValue('dni',wnew) ;
		},
		'form.email' : function(wnew,wold){
			this.validatingValue('email',wnew) ;
		},
		'form.estado' : function(wnew,wold){
			this.validatingValue('estado',wnew) ;
		},
		'form.all_customer' : function(wnew,wold){
			this.validatingValue('all_customer',wnew) ;
		},
		'form.all_orders' : function(wnew,wold){
			this.validatingValue('all_orders',wnew) ;
		},
	},
}) ;